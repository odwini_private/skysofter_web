<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'skysofter');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_ENV', "dev");

/**#@+ 
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'P=W+pmm15|~d**PydX]w;vx[=!`aG]VGVbH== ~u ]<dv![gasq1mw]JX_o&YH8M');
define('SECURE_AUTH_KEY',  '8=ZAuqcuk$O7Y/-MC,v]Cih%ZyPd3@`Z2n}J/|Ey^K%jW~lsD$RomtN$jff4HIh~');
define('LOGGED_IN_KEY',    'CV8tEVMOkH:Ci*{!QlCna!IHE(0kmf#4HMGb25 jQzC_n_mU-7^_BwY7cQu:rX9E');
define('NONCE_KEY',        'b.&ywx*W<x/6nt8x/s#WQ@;>N&JWe6HCr0y]fQ:+]-)I_LpHbD6v=MW(fB[hQ>P,');
define('AUTH_SALT',        ']p#b/O!-Q0U/Ke*}]/XS|Nu)k{#5qH>:_6cOkI2sh*NsB8S@ya9EVph~.&.xL.S?');
define('SECURE_AUTH_SALT', '5s)V|v36=q}}cd[DKg6qu*G(2fnG%YMh;KDR y{c~+ 9^GbChv*woE;33KIF,ziJ');
define('LOGGED_IN_SALT',   'T=eAZ;lC(Ba.mIn22sYw9SNtZvo=kp`uj4QKl675Mm?ck/dSy>:)oK!<-L>.0f[V');
define('NONCE_SALT',       'yA5SAt`ssgr~}WGzr3zix<:<}j&e837+Xw`/SJs&{ s.$MogqBeyGY)4ILJ+%]i5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
