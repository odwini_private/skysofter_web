<?php

class rest_for_acf
{
	/** @var rest_for_acfLoader $loader Maintains and registers all hooks for the plugin. */
	protected $loader;

	/** @var string $pluginName The string used to uniquely identify this plugin. */
	protected $pluginName;

	/** @var string $version The current version of the plugin. */
	protected $version;

	public function __construct() {
		if ( defined( 'REST_FOR_ACF_VERSION' ) ) {
			$this->version = REST_FOR_ACF_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->pluginName = 'rest_for_acf';

		$this->loadDependencies();
		$this->defineApiHooks();
		$this->definePublicHooks();
	}

	private function loadDependencies()
    {
        $pluginDirPath = plugin_dir_path(dirname(__FILE__));

		require_once $pluginDirPath . 'public/rest_for_acfPublic.php';
		require_once $pluginDirPath . 'api/rest_for_acfApi.php';
		require_once $pluginDirPath . 'includes/rest_for_acfLoader.php';

		$this->loader = new rest_for_acfLoader();
	}

    private function defineApiHooks()
    {
        $pluginApi = new rest_for_acfApi($this->getPluginName(), $this->getVersion());

        $this->loader->add_action('rest_api_init', $pluginApi, 'registerRoutes');
    }

    private function definePublicHooks()
    {
        $pluginPublic = new rest_for_acfPublic($this->getPluginName(), $this->getVersion());

        $this->loader->add_action('admin_enqueue_scripts', $pluginPublic, 'enqueueStyles');
        $this->loader->add_action('admin_enqueue_scripts', $pluginPublic, 'enqueueScripts');
    }

	public function run()
    {
		$this->loader->run();
	}

	public function getPluginName()
    {
		return $this->pluginName;
	}

	public function getLoader()
    {
		return $this->loader;
	}

	public function getVersion()
    {
		return $this->version;
	}

}