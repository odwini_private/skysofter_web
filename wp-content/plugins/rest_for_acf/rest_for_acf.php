<?php
/**
 * Plugin Name:       rest_for_acf
 * Description:       REST service for acf costume field
 * Version:           1.0.0
 * Author:            odwini
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PLUGIN_REST_FOR_ACF', '1.0.0' );

function activaterest_for_acf() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/rest_for_acfActivator.php';
    rest_for_acfActivator::activate();
}

function deactivaterest_for_acf() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/rest_for_acfDeactivator.php';
    rest_for_acfDeactivator::deactivate();
}

register_activation_hook( __FILE__, 'activaterest_for_acf' );
register_deactivation_hook( __FILE__, 'deactivaterest_for_acf' );

require plugin_dir_path( __FILE__ ) . 'includes/rest_for_acf.php';

(new rest_for_acf())->run();