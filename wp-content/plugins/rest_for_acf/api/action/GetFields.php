<?php

/**
 * Created by IntelliJ IDEA.
 * User: odwin
 * Date: 7/26/2018
 * Time: 9:55 PM
 */
class GetFields
{
    public function __construct(){

    }

    public function execute(WP_REST_Request $request)
    {
        if(isset($request['pageId'])){
           return $this->requestResource($request);
        }else if(isset($request['postType'])){
            $getCostumeFields = $request['includeCostumeFields'];
            $response = array();
            $limit = -1;
            $order = 'DESC';
            if(isset($request['limit']) && !empty($request['limit'])){
                $limit = intval($request['limit']);
            }
            if(isset($request['order']) && !empty($request['order'])){
                $order = $request['order'];
            }
            $options = array(
                'post_type'   => $request['postType'],
                'orderby'     => 'date',
                'order'       => 'DESC',
                'numberposts' => $limit
            );
            $posts = get_posts($options);
            if(isset($request['fields'])){
                foreach($posts as $post){
                    $post = (array) $post;
                    foreach ($request['fields'] as $field){
                        $post[$field] = get_post_meta($post['ID'], $field, TRUE);
                    }
                    $post['thumbnail_url'] = get_the_post_thumbnail_url($post['ID']);
                    if(isset($getCostumeFields) && !empty($getCostumeFields) && isset($request['custome_fields'])){
                        $post['custome_fields'] = $this->getCostumeFields($post['ID'], $request['custome_fields']);
                    }
                    $response[] = $post;
                }
            }else{
                foreach($posts as $post){
                    $post = (array) $post;
                    if(isset($getCostumeFields) && !empty($getCostumeFields) && isset($request['custome_fields'])){
                        $post['custome_fields'] = $this->getCostumeFields($post['ID'],$request['custome_fields']);
                    }
                    $post['thumbnail_url'] = get_the_post_thumbnail_url($post['ID']);
                    $response[] = $post;
                }
            }
            return new WP_REST_Response($response);
        }else{
            throw new Exception('Bad request');
        }
    }

    public function requestResource($request){
        $pageId = $request['pageId'];
        $includebaseInfo = $request['includeBaseInfo'];
        $getCostumeFields = $request['includeCostumeFields'];

        if(isset($request['fields'])){
            $response = array();
            $fields = $request['fields'];
            foreach ($fields as $field){
                $response[] = get_post_meta($pageId, $field, TRUE);
            }
            if(isset($includebaseInfo) && !empty($includebaseInfo)){
                $response[] = get_post($pageId, '', false);
            }
            if(isset($getCostumeFields) && !empty($getCostumeFields)){
                $response["custome_fields"] = $this->getCostumeFields($pageId, $request["custome_fields"]);
            }
            return new WP_REST_Response($response);
        }else{
            $response = (array) get_post($pageId, '', false);
            if(isset($getCostumeFields) && !empty($getCostumeFields)){
                $response["custome_fields"] = $this->getCostumeFields($pageId, $request["custome_fields"]);
            }
            return new WP_REST_Response($response);
        }
    }

    public function getCostumeFields($postId, $fields){
        $response = array();
            foreach ($fields as $field){
                $response[$field] = CFS()->get($field,$postId);
            }
        return $response;
    }

}