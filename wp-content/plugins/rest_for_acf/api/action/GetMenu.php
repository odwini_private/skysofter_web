<?php

/**
 * Created by IntelliJ IDEA.
 * User: odwin
 * Date: 7/28/2018
 * Time: 11:23 PM
 */
class GetMenu
{
    public function __construct(){

    }

    public function execute(WP_REST_Request $request)
    {
        $response = array();
        if(isset($request['navMenu'])){
            $response = wp_get_nav_menu_items($request['navMenu']);
        }
        return new WP_REST_Response($response);
    }

}