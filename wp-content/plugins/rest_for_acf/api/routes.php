<?php

include_once plugin_dir_path( __FILE__ ) . 'action/Ping.php';
include_once plugin_dir_path( __FILE__ ) . 'action/GetFields.php';
include_once plugin_dir_path( __FILE__ ) . 'action/GetMenu.php';

$namespace = 'rest_for_acf';

return [
    [
        'namespace' => $namespace,
        'route' => '/ping',
        'methods' => 'GET',
        'action' => "Ping",
    ],
    [
        'namespace' => $namespace,
        'route' => '/GetFields',
        'methods' => 'GET',
        'action' => "GetFields",
    ],
    [
        'namespace' => $namespace,
        'route' => '/GetMenu',
        'methods' => 'GET',
        'action' => "GetMenu",
    ],
];
