<?php

class rest_for_acfPublic
{
    /** @var string $pluginName The ID of this plugin. */
    private $pluginName;

    /** @var string $version The current version of this plugin. */
    private $version;

    /**
     * @param string $pluginName The name of this plugin.
     * @param string $version The version of this plugin.
     */
    public function __construct($pluginName, $version)
    {
        $this->pluginName = $pluginName;
        $this->version = $version;
    }

	public function enqueueStyles()
    {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/rest_for_acfPublic.css', array(), $this->version, 'all' );
	}

	public function enqueueScripts()
    {
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/rest_for_acfPublic.js', array( 'jquery' ), $this->version, false );
	}
}
