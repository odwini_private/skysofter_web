<?php
/**
 * Template Name: Main page
 *
 * @package SkySofter
 * @subpackage SkySofter
 * @since SkySofter 1.0
 */
get_header();
$nav_response = wp_get_nav_menu_items("Top nav");


$news_options = array(
    'post_type'   => "post",
    'orderby'     => 'date',
    'order'       => 'DESC',
    'numberposts' => -1
);
$posts = get_posts($options);
if(isset($request['fields'])){
    foreach($posts as $post){
        $post = (array) $post;
        foreach ($request['fields'] as $field){
            $post[$field] = get_post_meta($post['ID'], $field, TRUE);
        }
        $post['thumbnail_url'] = get_the_post_thumbnail_url($post['ID']);
        if(isset($getCostumeFields) && !empty($getCostumeFields) && isset($request['custome_fields'])){
            $post['custome_fields'] = $this->getCostumeFields($post['ID'], $request['custome_fields']);
        }
        $news_response[] = $post;
    }
}else{
    foreach($posts as $post){
        $post = (array) $post;
        if(isset($getCostumeFields) && !empty($getCostumeFields) && isset($request['custome_fields'])){
            $post['custome_fields'] = $this->getCostumeFields($post['ID'],$request['custome_fields']);
        }
        $post['thumbnail_url'] = get_the_post_thumbnail_url($post['ID']);
        $news_response[] = $post;
    }
}


$news_options = array(
    'post_type'   => "post",
    'orderby'     => 'date',
    'order'       => 'DESC',
    'numberposts' => -1
);
$posts = get_posts($options);
if(isset($request['fields'])){
    foreach($posts as $post){
        $post = (array) $post;
        foreach ($request['fields'] as $field){
            $post[$field] = get_post_meta($post['ID'], $field, TRUE);
        }
        $post['thumbnail_url'] = get_the_post_thumbnail_url($post['ID']);
        if(isset($getCostumeFields) && !empty($getCostumeFields) && isset($request['custome_fields'])){
            $post['custome_fields'] = $this->getCostumeFields($post['ID'], $request['custome_fields']);
        }
        $news_response[] = $post;
    }
}else{
    foreach($posts as $post){
        $post = (array) $post;
        if(isset($getCostumeFields) && !empty($getCostumeFields) && isset($request['custome_fields'])){
            $post['custome_fields'] = $this->getCostumeFields($post['ID'],$request['custome_fields']);
        }
        $post['thumbnail_url'] = get_the_post_thumbnail_url($post['ID']);
        $news_response[] = $post;
    }
}

?>
    <script>
        window.news = <?php print_r(json_encode(new WP_REST_Response($news_response))); ?>;
        window.main_nav = <?php  print_r(json_encode(new WP_REST_Response($nav_response))); ?>;
        window.projects = <?php  print_r(getFields(array(
                                "postType"=>"projects",
                                "includeCostumeFields"=>"true",
                                "order"=>"DESC",
                                "custome_fields"=> array(
                                        0 => 'project_short_description',
                                        1 => 'projects_category',
                                        2 => 'technologies'
                                )
                            )
        )); ?>;
        window.clients_partners = <?php  print_r(getFields(array(
                "postType"=>"cliets_and_partners",
                "fields"=> array(
                    0 => 'url'
                )
            )
        )); ?>;
        window.contact_info = <?php  print_r(getFields(array(
                "pageId"=>"2",
                "includeCostumeFields"=>"true",
                "custome_fields"=> array(
                    0 => 'main_social_links',
                    1 => 'company_info'
                )
            )
        )); ?>;
        window.services = <?php  print_r(getFields(array(
                "pageId"=>"2",
                "includeCostumeFields"=>"true",
                "custome_fields"=> array(
                    0 => 'services'
                )
            )
        )); ?>;
        window.team = <?php  print_r(getFields(array(
                "postType"=>"team",
                "includeCostumeFields"=>"true",
                "limit"=>"4",
                "order"=>"ASC",
                "custome_fields"=> array(
                    0 => 'team_short_description',
                    1 => 'team_position',
                    2 => 'social_icon'
                )
            )
        )); ?>;
    </script>

    <?php
        get_template_part('template-parts/main-nav');
    ?>

    <my-app></my-app>

<?php get_footer(); ?>