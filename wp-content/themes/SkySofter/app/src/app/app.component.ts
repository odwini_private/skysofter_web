import { Component, OnInit, Inject } from '@angular/core';
import { APP_BASE_HREF } from "@angular/common";
import {AppSettings} from '../AppSettings';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styles:[
      require('./common.scss').toString(),
      require('./app.component.scss').toString()
  ]
})
export class AppComponent implements OnInit {
    public appThemeHref: any;

  constructor(@Inject(APP_BASE_HREF) public baseHref: string) {
      let appSetting = new AppSettings();
      this.appThemeHref = appSetting.APP_THEME_HREF;
  }

  ngOnInit() {

  }
}
