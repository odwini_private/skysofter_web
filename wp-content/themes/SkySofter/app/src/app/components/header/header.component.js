import { Component, ElementRef, HostListener, Inject } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HeaderInfoService } from './header-info.service';
import { DOCUMENT } from "@angular/platform-browser";
import { WINDOW } from "../window/window.service";
export class HeaderComponent {
    constructor(document, window, el, _HeaderInfoService) {
        this.document = document;
        this.window = window;
        this.el = el;
        this._HeaderInfoService = _HeaderInfoService;
        let appSetting = new AppSettings();
        this.themeHref = appSetting.APP_THEME_HREF;
        this.headerInfo = _HeaderInfoService.getServices();
    }
    ngOnInit() {
        if (window.innerWidth > 1250) {
            this.setSizeOfHeader();
        }
        else {
            this.removeSizeOfHeader();
        }
    }
    setSizeOfHeader() {
        // let moveBg = new moveBackground(".header-bg_floating-particles");
        let height = $(".header").height();
        $(this.el.nativeElement).css("min-height", height + "px");
    }
    removeSizeOfHeader() {
        $(this.el.nativeElement).css("min-height", "");
    }
    onWindowResize() {
        if (window.innerWidth > 1250) {
            this.setSizeOfHeader();
        }
        else {
            this.removeSizeOfHeader();
        }
    }
    ngAfterViewInit() {
        $(this.el.nativeElement).find(".sample").addClass("make-magic");
    }
    onWindowScroll() {
        let number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
        if (number > 30) {
            $(".header .right-content").addClass("animated");
        }
    }
}
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'header-section',
                templateUrl: './header.component.html',
                styles: [require('./header.component.scss').toString()],
                providers: [HeaderInfoService]
            },] },
];
/** @nocollapse */
HeaderComponent.ctorParameters = () => [
    { type: Document, decorators: [{ type: Inject, args: [DOCUMENT,] },] },
    { type: Window, decorators: [{ type: Inject, args: [WINDOW,] },] },
    { type: ElementRef, },
    { type: HeaderInfoService, },
];
HeaderComponent.propDecorators = {
    "onWindowResize": [{ type: HostListener, args: ["window:resize", [],] },],
    "onWindowScroll": [{ type: HostListener, args: ["window:scroll", [],] },],
};
//# sourceMappingURL=header.component.js.map