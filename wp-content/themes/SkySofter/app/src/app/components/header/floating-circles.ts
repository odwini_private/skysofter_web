export class moveBackground {

    public lFollowX: number;
    public lFollowY: number;
    public x: number;
    public y: number;
    public friction: number;

    constructor(selector = ""){
        let that = this;
        this.lFollowX = 0;
        this.lFollowY = 0;
        this.x = 0;
        this.y = 0;
        this.friction = 1 / 30;
        jQuery(window).on('mousemove click', function(e: any) {
            var lMouseX = Math.max(-100, Math.min(100, jQuery(window).width() / 2 - e.clientX));
            var lMouseY = Math.max(-100, Math.min(100, jQuery(window).height() / 2 - e.clientY));
            that.lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
            that.lFollowY = (10 * lMouseY) / 100;
            that.moveBackground(selector);
        });
    }

    moveBackground(selector = "") {
      var that = this;
      this.x += (this.lFollowX - this.x) * this.friction;
      this.y += (this.lFollowY - this.y) * this.friction;

      var translate = 'translate(' + this.x + 'px, ' + this.y + 'px) scale(1.1)';

      jQuery(selector).css({
            '-webit-transform': translate,
            '-moz-transform': translate,
            'transform': translate
      });
    }
}

