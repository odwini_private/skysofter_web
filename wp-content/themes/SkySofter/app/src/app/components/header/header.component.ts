import { Component, OnInit, ElementRef, HostListener, Inject } from '@angular/core';
import {AppSettings} from '../../../AppSettings';
import { moveBackground } from "./floating-circles";
import {HeaderInfoService} from './header-info.service';
import { DOCUMENT } from "@angular/platform-browser";
import { WINDOW } from "../window/window.service";


@Component({
    selector: 'header-section',
    templateUrl: './header.component.html',
    styles:[require('./header.component.scss').toString()],
    providers: [HeaderInfoService]
})
export class HeaderComponent implements OnInit {
    public themeHref: string;
    public headerInfo: any;

    constructor(@Inject(DOCUMENT) private document: Document, @Inject(WINDOW) private window: Window, private el: ElementRef, public _HeaderInfoService: HeaderInfoService) {
        let appSetting = new AppSettings();
        this.themeHref = appSetting.APP_THEME_HREF;
        this.headerInfo = _HeaderInfoService.getServices();
    }

    ngOnInit() {
        if(window.innerWidth > 1250){
            this.setSizeOfHeader();
        }else{
            this.removeSizeOfHeader();
        }
    }

    setSizeOfHeader(){
        // let moveBg = new moveBackground(".header-bg_floating-particles");
        let height = $(".header").height();
        $(this.el.nativeElement).css("min-height", height+"px");
    }

    removeSizeOfHeader(){
        $(this.el.nativeElement).css("min-height", "");
    }


    @HostListener("window:resize", [])
    onWindowResize() {
       if(window.innerWidth > 1250){
           this.setSizeOfHeader();
       }else{
           this.removeSizeOfHeader();
       }
    }

    ngAfterViewInit(){
        $(this.el.nativeElement).find(".sample").addClass("make-magic");
    }

    @HostListener("window:scroll", [])
    onWindowScroll() {
        let number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
        if (number > 30) {
            $(".header .right-content").addClass("animated");
        }
    }
}
