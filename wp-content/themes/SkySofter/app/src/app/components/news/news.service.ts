import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class NewsService {
    public news: any;

    constructor(private httpClient: HttpClient){}

    getNews(): any {
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF+"/wp-json/rest_for_acf/GetFields";
        let headers = new HttpHeaders();
        let params: HttpParams = new HttpParams();
        params = params.set("postType", "post");
        params = params.set("order", 'DESC');
        return this.httpClient.get(url, { headers, params });
    }

} 