import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
export class NewsService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getNews() {
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF + "/wp-json/rest_for_acf/GetFields";
        let headers = new HttpHeaders();
        let params = new HttpParams();
        params = params.set("postType", "post");
        params = params.set("order", 'DESC');
        return this.httpClient.get(url, { headers, params });
    }
}
NewsService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
NewsService.ctorParameters = () => [
    { type: HttpClient, },
];
//# sourceMappingURL=news.service.js.map