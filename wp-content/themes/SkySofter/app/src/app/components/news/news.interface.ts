export interface News {
    sample: string;
    title: string;
    url: string;
    date: any;
    description: string;
}
