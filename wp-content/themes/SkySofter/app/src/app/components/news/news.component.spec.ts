import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { NewsComponent } from './news.component';

describe('News', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [NewsComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(NewsComponent);
        expect(fixture.componentInstance instanceof NewsComponent).toBe(
            true,
            'should create AppComponent'
        );
    });
});
