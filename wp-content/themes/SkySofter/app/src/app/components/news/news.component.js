import { Component, Inject, ViewChild, ElementRef } from '@angular/core';
import { APP_BASE_HREF } from "@angular/common";
import { AppSettings } from '../../../AppSettings';
import { ContentService } from "./content.service";
import { OwlCarouselComponent } from '../owl-carousel/owl-carousel.component';
export class NewsComponent {
    constructor(baseHref, _contentService, el) {
        this.baseHref = baseHref;
        this._contentService = _contentService;
        this.el = el;
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
        this.news = window["news"].data;
        this.content = this.getContent();
        this.el = el;
    }
    ngOnInit() {
        this.carouselOptions = {
            navigation: true,
            pagination: false,
            smartSpeed: 1200,
            fluidSpeed: 500,
            rewindNav: false,
            margin: 25,
            loop: true,
            items: 2,
            responsive: {
                250: {
                    items: 1,
                    navigation: false,
                    pagination: true,
                    margin: 0
                },
                651: {
                    items: 1.3
                },
                750: {
                    items: 1.4
                },
                850: {
                    items: 1.6
                },
                1100: {
                    items: 1.8
                },
                1260: {
                    items: 2
                },
                1560: {
                    items: 2.1
                }
            }
        };
        this.pinEventsToCostumeArrows();
    }
    getContent() {
        let subscription = this._contentService.getServices().subscribe(data => { this.content = data; subscription.unsubscribe(); }, err => console.error(err));
    }
    pinEventsToCostumeArrows() {
        let that = this;
        $(this.el.nativeElement).find(".right_arrow").click(function () {
            that.owlCarouselComponent.$owlElement.trigger('next.owl.carousel');
        });
        $(this.el.nativeElement).find(".left_arrow").click(function () {
            that.owlCarouselComponent.$owlElement.trigger('prev.owl.carousel');
        });
    }
}
NewsComponent.decorators = [
    { type: Component, args: [{
                selector: 'news',
                templateUrl: './news.component.html',
                styles: [require('./news.component.scss').toString()],
                providers: [ContentService]
            },] },
];
/** @nocollapse */
NewsComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [APP_BASE_HREF,] },] },
    { type: ContentService, },
    { type: ElementRef, },
];
NewsComponent.propDecorators = {
    "owlCarouselComponent": [{ type: ViewChild, args: [OwlCarouselComponent,] },],
};
//# sourceMappingURL=news.component.js.map