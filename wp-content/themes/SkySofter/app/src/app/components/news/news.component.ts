import {Component, OnInit, Inject, ViewChild, ElementRef} from '@angular/core';
import {APP_BASE_HREF} from "@angular/common";
import {AppSettings} from '../../../AppSettings';
import {ContentService} from "./content.service";
import {OwlCarouselComponent} from '../owl-carousel/owl-carousel.component';

@Component({
    selector: 'news',
    templateUrl: './news.component.html',
    styles: [require('./news.component.scss').toString()],
    providers: [ ContentService]
})
export class NewsComponent implements OnInit {

    @ViewChild(OwlCarouselComponent) owlCarouselComponent: OwlCarouselComponent;
    public appThemeHref: string;
    public news: any;
    public carouselOptions: any;
    public content: any;

    constructor(@Inject(APP_BASE_HREF) public baseHref: string,
                public _contentService: ContentService,
                private el: ElementRef) {
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
        this.news = window["news"].data;
        this.content = this.getContent();
        this.el = el;
    }

    ngOnInit() {
        this.carouselOptions = {
            navigation: true,
            pagination: false,
            smartSpeed: 1200,
            fluidSpeed: 500,
            rewindNav: false,
            margin: 25,
            loop: true,
            items: 2,
            responsive: {
                250: {
                    items: 1,
                    navigation: false,
                    pagination: true,
                    margin: 0
                },
                651:{
                    items: 1.3
                },
                750: {
                    items: 1.4
                },
                850:{
                    items: 1.6
                },
                1100: {
                    items: 1.8
                },
                1260: {
                    items: 2
                },
                1560: {
                    items: 2.1
                }
            }
        };
        this.pinEventsToCostumeArrows();
    }


    getContent(){
        let subscription = this._contentService.getServices().subscribe(
            data => { this.content = data; subscription.unsubscribe();},
            err => console.error(err)
        );
    }

    pinEventsToCostumeArrows() {
        let that = this;
        $(this.el.nativeElement).find(".right_arrow").click(function () {
            that.owlCarouselComponent.$owlElement.trigger('next.owl.carousel');
        })
        $(this.el.nativeElement).find(".left_arrow").click(function () {
            that.owlCarouselComponent.$owlElement.trigger('prev.owl.carousel');
        })

    }
}
