import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class servicesService {
    public services: any = [
        {
            service_image: "/assets/images/services/cloud.png",
            service_heading: "Cloud",
            service_description: "Machine Learning data processing, data analytics"
        },
        {
            service_image: "/assets/images/services/web.png",
            service_heading: "Web",
            service_description: "Websites, Dedicated web applications, Scalable"
        },
        {
            service_image: "/assets/images/services/UiUx.png",
            service_heading: "UX/UI",
            service_description: "User friendly interfaces, A/B test, statistics and social survey"
        },
        {
            service_image: "/assets/images/services/mobile.png",
            service_heading: "Mobile",
            service_description: "Cross platform mobile applications iOS/Android/Windows support Native and canvas based apps"
        },
        {
            service_image: "/assets/images/services/desktop.png",
            service_heading: "Desktop",
            service_description: "Machine Learning data processing Data analytics"
        }
    ];

    constructor(private httpClient: HttpClient){}

    getServices(): any {
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF+"/wp-json/rest_for_acf/GetFields";
        let headers = new HttpHeaders();
        let params: HttpParams = new HttpParams();
        params = params.set("pageId", "2");
        params = params.set("includeCostumeFields", 'true');
        params = params.set("custome_fields[0]", 'services');
        return this.httpClient.get(url, { headers, params });
    }
} 