import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
export class ContentService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getServices() {
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF + "/wp-json/rest_for_acf/GetFields";
        let headers = new HttpHeaders();
        let params = new HttpParams();
        params = params.set("pageId", window["thisPost"].data.ID);
        params = params.set("fields[0]", 'services_heading');
        params = params.set("fields[1]", 'services_description');
        return this.httpClient.get(url, { headers, params });
    }
}
ContentService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ContentService.ctorParameters = () => [
    { type: HttpClient, },
];
//# sourceMappingURL=content.service.js.map