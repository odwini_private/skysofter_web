export interface ServiceType {
    icon: string;
    title: string;
    description: string;
}