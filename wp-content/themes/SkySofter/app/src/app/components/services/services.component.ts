import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ContentService } from './content.service';
import { AppSettings } from '../../../AppSettings';
import {OwlCarouselComponent} from '../owl-carousel/owl-carousel.component';
import {Observable} from 'rxjs/Rx';
// import { CirclesComponent } from '../circles/circles.component';

@Component({
    selector: 'services-section',
    templateUrl: './services.component.html',
    styles:[require('./services.component.scss').toString()],
    providers: [ ContentService]
})
export class ServicesComponent implements OnInit {
    @ViewChild(OwlCarouselComponent) owlCarouselComponent: OwlCarouselComponent;
    public themeHref: string;
    public services: any;
    public carouselOptions: Object;
    public initWindowSize: number;
    public destroyWindowSize: number;
    public HeaderInfo: any;
    public content: any;

    constructor( public _contentService: ContentService, private el: ElementRef) {
        let appSetting = new AppSettings();
        this.themeHref = appSetting.APP_THEME_HREF;
        this.el = el;
        this.services = window["services"].data.custome_fields.services;
        this.content = this.getContent();
        this.initWindowSize = 1250;
        this.destroyWindowSize = 650;
        this.carouselOptions = {
            navigation: false,
            pagination: false,
            stagePagging: 18,
            smartSpeed: 1200,
            fluidSpeed: 500,
            rewindNav: false,
            margin: 18,
            loop: true,
            items: 2.5,
            responsive: {
                750: {
                    items: 2.5
                },
                950: {
                    items: 3
                },
                1250: {
                    items: 4
                },
                1560: {
                    items: 4
                }
            }
        };
        this.HeaderInfo = window["thisPost"].data;
    }


    getContent(){
      let subscription = this._contentService.getServices().subscribe(
               data => { this.content = data; subscription.unsubscribe();},
               err => console.error(err)
        );
    }

    ngAfterViewInit() {
        this.pinEventsToCostumeArrows();
    }

    pinEventsToCostumeArrows() {
        let that = this;
        $(this.el.nativeElement).find(".right_arrow").click(function () {
            that.owlCarouselComponent.$owlElement.trigger('next.owl.carousel');
        });
        $(this.el.nativeElement).find(".left_arrow").click(function () {
            that.owlCarouselComponent.$owlElement.trigger('prev.owl.carousel');
        });
    }

    ngOnInit() {}
}
