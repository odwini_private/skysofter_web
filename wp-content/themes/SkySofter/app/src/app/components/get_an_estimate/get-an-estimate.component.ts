import {Component, OnInit, Inject, ViewChild} from '@angular/core';
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import {
    ReactiveFormsModule,
    FormsModule,
    FormGroup,
    FormControl,
    Validators,
    FormBuilder
} from '@angular/forms';
import {APP_BASE_HREF} from "@angular/common";
// import {CirclesComponent} from '../circles/circles.component';
import {AppSettings} from '../../../AppSettings';

@Component({
    selector: 'get-an-estimate',
    templateUrl: './get-an-estimate.component.html',
    styles: [require('./get-an-estimate.component.scss').toString()],
})
export class GetAnEstimateComponent implements OnInit {

    public appThemeHref: string;
    // @ViewChild(CirclesComponent) CirclesComponent: CirclesComponent;
    myform: FormGroup;
    name: FormControl;
    email: FormControl;
    content: FormControl;

    constructor(@Inject(APP_BASE_HREF) public baseHref: string) {
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
    }

    ngOnInit() {
        this.createFormControls();
        this.createForm();

    }

    ngAfterViewInit() {
        // this.CirclesComponent.ciclesCanvas.forceStopDrawing();
        let that = this;
        $(".get_an_estimate-button").click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(".get-an-estimate").fadeIn(300, function () {
                disableBodyScroll( $(".get-an-estimate")[0]);
                $("body").toggleClass("scroll-lock");
                // that.CirclesComponent.ciclesCanvas.forceRedrawCanvas();
            });
        });
        $(".close_estimate").click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            // that.CirclesComponent.ciclesCanvas.forceStopDrawing();
            let localTimeout = setTimeout(function(){
                $(".get-an-estimate").fadeOut(300,function(){});
                enableBodyScroll($(".get-an-estimate")[0]);
                $("body").removeClass("scroll-lock");
                clearTimeout(localTimeout);
            },100);

        });
    }

    createFormControls() {
        this.name = new FormControl('', [
            Validators.required,
            Validators.minLength(2)
        ]);
        this.email = new FormControl('', [
            Validators.required,
            Validators.pattern("[^ @]*@[^ @]*")
        ]);
        this.content = new FormControl('', [
            Validators.required,
            Validators.minLength(8)
        ]);
    }

    createForm() {
        this.myform = new FormGroup({
            name: this.name,
            email: this.email,
            content: this.content,
        });
    }

    submitForm() {
        if (this.myform.status === "VALID") {
            let form = this.myform;
            $.post("mail.php", form.value)
                .done(function (data: any) {
                    alert("Thank you for replay!");
                    form.reset();
                    $(".get-an-estimate").fadeOut(300,function(){});
                    enableBodyScroll($(".get-an-estimate")[0]);
                    $("body").removeClass("scroll-lock");
                    // this.CirclesComponent.ciclesCanvas.forceStopDrawing();
                });
        }
    }
}
