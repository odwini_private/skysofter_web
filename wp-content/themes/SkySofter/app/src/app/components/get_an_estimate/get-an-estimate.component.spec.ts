import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { GetAnEstimateComponent } from './get-an-estimate.component';

describe('get-an-estimate', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [GetAnEstimateComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(GetAnEstimateComponent);
        expect(fixture.componentInstance instanceof GetAnEstimateComponent).toBe(
            true,
            'should create AppComponent'
        );
    });
});
