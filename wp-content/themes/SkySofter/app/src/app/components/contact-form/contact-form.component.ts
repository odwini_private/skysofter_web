import { Component, OnInit, Inject,  } from '@angular/core';
import { ReactiveFormsModule,
    FormsModule,
    FormGroup,
    FormControl,
    Validators,
    FormBuilder } from '@angular/forms';
import { APP_BASE_HREF } from "@angular/common";
import {AppSettings} from '../../../AppSettings';

@Component({
    selector: 'contact-form',
    templateUrl: './contact-form.component.html',
    styles:[require('./contact-form.component.scss').toString()]
})

export class ContactFormComponent implements OnInit {
    public appThemeHref: string;
    myform: FormGroup;
    name: FormControl;
    email: FormControl;
    content: FormControl;

    constructor(@Inject(APP_BASE_HREF) public baseHref: string) {
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
    }

    ngOnInit() {
        this.createFormControls();
        this.createForm();
    }

    createFormControls() {
        this.name = new FormControl('', [
            Validators.required,
            Validators.minLength(2)
        ]);
        this.email = new FormControl('', [
            Validators.required,
            Validators.pattern("[^ @]*@[^ @]*")
        ]);
        this.content = new FormControl('', [
            Validators.required,
            Validators.minLength(8)
        ]);
    }

    createForm() {
        this.myform = new FormGroup({
            name: this.name,
            email: this.email,
            content: this.content,
        });
    }

    submitForm(){
        if(this.myform.status === "VALID"){
            let form = this.myform;
            $.post( "mail.php", form.value)
                .done(function( data: any ) {
                    console.log(data);
                    alert( "Thank you for replay!");
                    form.reset();
                });
        }
    }
}
