import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { ContactFormComponent } from './contact-form.component';

describe('get-an-estimate', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [ContactFormComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(ContactFormComponent);
        expect(fixture.componentInstance instanceof ContactFormComponent).toBe(
            true,
            'should create AppComponent'
        );
    });
});
