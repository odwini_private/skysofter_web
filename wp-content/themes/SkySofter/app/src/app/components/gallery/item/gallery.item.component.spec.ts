import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { GalleryItemComponent } from './gallery.item.component';

describe('Gallery', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [GalleryItemComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(GalleryItemComponent);
        expect(fixture.componentInstance instanceof GalleryItemComponent).toBe(
            true,
            'should create AppComponent'
        );
    });
});
