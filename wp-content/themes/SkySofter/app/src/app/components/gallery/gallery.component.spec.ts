import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { GalleryComponent } from './gallery.component';

describe('Gallery', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [GalleryComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(GalleryComponent);
        expect(fixture.componentInstance instanceof GalleryComponent).toBe(
            true,
            'should create AppComponent'
        );
    });
});
