import { Component, OnInit, Inject } from '@angular/core';
import { APP_BASE_HREF } from "@angular/common";
import {AppSettings} from '../../../AppSettings';

@Component({
    selector: 'gallery-section',
    templateUrl: './gallery.component.html',
    styles:[require('./gallery.component.scss').toString()]
})
export class GalleryComponent implements OnInit {
    public appThemeHref: string
    constructor(@Inject(APP_BASE_HREF) public baseHref: string) {
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
    }

    ngOnInit() {}
}
