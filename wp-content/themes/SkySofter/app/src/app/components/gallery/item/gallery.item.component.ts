import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'gallery-item',
    templateUrl: './gallery.item.component.html',
    styles:[require('./gallery.item.component.scss').toString()]
})
export class GalleryItemComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}
