import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import {ContentService} from "./content.service";
import {OwlCarouselComponent} from '../owl-carousel/owl-carousel.component';

@Component({
    selector: 'team-section',
    templateUrl: './team.component.html',
    styles:[require('./team.component.scss').toString()],
    providers: [ ContentService]
})
export class TeamComponent implements OnInit {
    @ViewChild(OwlCarouselComponent) owlCarouselComponent: OwlCarouselComponent;
    public themeHref: string;
    public team: any;
    public carouselOptions: Object;
    public initWindowSize: number;
    public destroyWindowSize: number;
    public content: any;

    constructor(public _contentService: ContentService,  private el: ElementRef) {
        let appSetting = new AppSettings();
        this.themeHref = appSetting.APP_THEME_HREF;
        this.el = el;
        this.team = window["team"].data;
        this.content = this.getContent();
        this.initWindowSize = 1250;
        this.destroyWindowSize = 650;
        this.carouselOptions = {
            navigation: false,
            pagination: false,
            smartSpeed: 1200,
            fluidSpeed: 500,
            rewindNav: false,
            margin: 20,
            stagePadding: 0,
            loop: true,
            items: 1.35,
            responsive: {
                650: {
                    items: 1.05
                },
                750:{
                    items: 1.25
                },
                850: {
                    items: 1.45
                },
                1000:{
                    items: 1.65
                },
                1100:{
                    items: 1.75
                },
                1250: {
                    items: 2.3
                },
                1560: {
                    items: 1.35
                }
            }
        };

    }

    ngAfterViewInit() {
        this.pinEventsToCostumeArrows();
    }

    getContent(){
       let subscription = this._contentService.getServices().subscribe(
            data => { this.content = data; subscription.unsubscribe();},
            err => console.error(err)
        );
    }

    pinEventsToCostumeArrows() {
        let that = this;
        $(this.el.nativeElement).find(".right_arrow").click(function () {
            that.owlCarouselComponent.$owlElement.trigger('next.owl.carousel');
        });
        $(this.el.nativeElement).find(".left_arrow").click(function () {
            that.owlCarouselComponent.$owlElement.trigger('prev.owl.carousel');
        });
    }

    ngOnInit() {}
}
