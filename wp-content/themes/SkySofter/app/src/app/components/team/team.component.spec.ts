import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { TeamComponent } from './team.component';

describe('Team', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [TeamComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(TeamComponent);
        expect(fixture.componentInstance instanceof TeamComponent).toBe(
            true,
            'should create ServicesComponent'
        );
    });
});
