import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class EmploeeService {
    public emploees: any = [
        {
            photo: "/assets/images/team/patryk.png",
            name: "Patryk Ciebień",
            position: "CEO and Founder",
            description: "IT expert of software design and development. He has many years" +
            "of experience both as a dev...",
            social: [
                {
                    icon: "fa-facebook-f",
                    href: "#"
                },
                {
                    icon: "fa-linkedin",
                    href: "#"
                },
                {
                    icon: "fa-twitter",
                    href: "#"
                },
                {
                    icon: "fa-google-plus",
                    href: "#"
                }
            ]
        },
        {
            photo: "/assets/images/team/Kasia.png",
            name: "Katarzyna Sęktas",
            position: "Founder",
            description: "She is a co-owner of SkySofter – a technology company and a softwarehouse. She has advised...",
            social: [
               {
                    icon: "fa-facebook-f",
                    href: "#"
                },
                 {
                    icon: "fa-linkedin",
                    href: "#"
                },
                 {
                    icon: "fa-twitter",
                    href: "#"
                },
                {
                    icon: "fa-google-plus",
                    href: "#"
                }
            ]
        },
        {
            photo: "/assets/images/team/slavomir.png",
            name: "Sławomir Świętochowski",
            position: "Founder",
            description: "A specialist in the company’s development and project management. An expert in...",
            social: [
                {
                    icon: "fa-facebook-f",
                    href: "#"
                },
                {
                    icon: "fa-linkedin",
                    href: "#"
                },
                {
                    icon: "fa-twitter",
                    href: "#"
                },
                {
                    icon: "fa-google-plus",
                    href: "#"
                }
            ]
        },
        {
            photo: "/assets/images/team/sebastian.png",
            name: "Sebastian Rogala",
            position: "PHP Master Engineer",
            description: "loves to tinker with code in PHP. He’s like a code sniper. Picks different mechanisms and fixes them...",
            social: [
                {
                    icon: "fa-facebook-f",
                    href: "#"
                },
                {
                    icon: "fa-linkedin",
                    href: "#"
                },
                {
                    icon: "fa-twitter",
                    href: "#"
                },
                {
                    icon: "fa-google-plus",
                    href: "#"
                }
            ]
        }
    ];

    constructor(private httpClient: HttpClient){}

    getEmployees(): any{
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF+"/wp-json/rest_for_acf/GetFields";
        let headers = new HttpHeaders();
        let params: HttpParams = new HttpParams();
        params = params.set("postType", "team");
        params = params.set("includeCostumeFields", 'true');
        params = params.set("limit", '4');
        params = params.set("order", 'ASC');
        params = params.set("custome_fields[0]", 'team_short_description');
        params = params.set("custome_fields[1]", 'team_position');
        params = params.set("custome_fields[2]", 'social_icon');
        return this.httpClient.get(url, { headers, params });
    }
} 