export interface Emploee {
    photo: string;
    name: string;
    position: string;
    description: string;
    social: any;
}
