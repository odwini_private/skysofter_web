import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ContentService {
    public Content: any;

    constructor(private httpClient: HttpClient) { }

    getServices(): any {
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF+"/wp-json/rest_for_acf/GetFields";
        let headers = new HttpHeaders();

        let params: HttpParams = new HttpParams();
        params = params.set("pageId", window["thisPost"].data.ID);
        params = params.set("fields[0]", 'team_heading');
        params = params.set("fields[1]", 'team_description');

        return this.httpClient.get(url, { headers, params });
    }
} 