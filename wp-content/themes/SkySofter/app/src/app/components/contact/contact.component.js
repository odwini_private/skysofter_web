import { Component, Inject, } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { APP_BASE_HREF } from "@angular/common";
import { AppSettings } from '../../../AppSettings';
export class ContactComponent {
    constructor(baseHref) {
        this.baseHref = baseHref;
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
        this.contactInfo = window["contact_info"].data;
    }
    ngOnInit() {
        this.createFormControls();
        this.createForm();
        this.pinEvents();
    }
    createFormControls() {
        this.name = new FormControl('', [
            Validators.required,
            Validators.minLength(2),
        ]);
        this.email = new FormControl('', [
            Validators.required,
            Validators.pattern("[^ @]*@[^ @]*")
        ]);
        this.content = new FormControl('', [
            Validators.required,
            Validators.minLength(8)
        ]);
    }
    createForm() {
        this.myform = new FormGroup({
            name: this.name,
            email: this.email,
            content: this.content,
        });
    }
    submitForm() {
        if (this.myform.status === "VALID") {
            let form = this.myform;
            $.post("mail.php", form.value)
                .done(function (data) {
                alert("Thank you for replay!");
                form.reset();
            });
        }
    }
    pinEvents() {
        $(".show_map-targe-box").click(function (e) {
            $(".contact .middle_circles").fadeOut(function () {
                $(".contact").addClass("show_map");
            });
        });
        $(".close_map").click(function (e) {
            $(".contact").removeClass("show_map");
            let fadeTimeout = setTimeout(function () {
                $(".contact .middle_circles").fadeIn(function () { });
            }, 1000);
        });
    }
}
ContactComponent.decorators = [
    { type: Component, args: [{
                selector: 'contact',
                templateUrl: './contact.component.html',
                styles: [require('./contact.component.scss').toString()],
            },] },
];
/** @nocollapse */
ContactComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [APP_BASE_HREF,] },] },
];
//# sourceMappingURL=contact.component.js.map