import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
export class ContactInfoService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getContactInfo() {
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF + "/wp-json/rest_for_acf/GetFields";
        let headers = new HttpHeaders();
        let params = new HttpParams();
        params = params.set("pageId", "2");
        params = params.set("includeCostumeFields", 'true');
        params = params.set("custome_fields[0]", 'main_social_links');
        params = params.set("custome_fields[1]", 'company_info');
        return this.httpClient.get(url, { headers, params });
    }
}
ContactInfoService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ContactInfoService.ctorParameters = () => [
    { type: HttpClient, },
];
//# sourceMappingURL=contact-info.service.js.map