import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { ContactComponent } from './contact.component';

describe('Circles', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [ContactComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(ContactComponent);
        expect(fixture.componentInstance instanceof ContactComponent).toBe(
            true,
            'should create AppComponent'
        );
    });
});
