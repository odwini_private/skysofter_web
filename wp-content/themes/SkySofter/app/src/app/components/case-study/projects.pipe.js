import { Pipe } from '@angular/core';
export class ProjectsPipe {
    transform(items, searchCategory) {
        if (!items)
            return [];
        if (!searchCategory)
            return items;
        searchCategory = searchCategory.toLowerCase();
        return items.filter(it => {
            return it.custome_fields.projects_category.toLowerCase().includes(searchCategory);
        });
    }
}
ProjectsPipe.decorators = [
    { type: Pipe, args: [{
                name: 'projects'
            },] },
];
//# sourceMappingURL=projects.pipe.js.map