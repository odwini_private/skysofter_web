import { Pipe, PipeTransform } from '@angular/core';
import {Project} from "./project.interface";
@Pipe({
    name: 'projects'
})
export class ProjectsPipe implements PipeTransform {
    transform(items: any, searchCategory: string): any {
        if(!items) return [];
        if(!searchCategory) return items;
        searchCategory = searchCategory.toLowerCase();
        return items.filter( it => {
            return it.custome_fields.projects_category.toLowerCase().includes(searchCategory);
        });
    }
}