import { Component, OnInit, Input } from '@angular/core';
import { AppSettings } from '../../../AppSettings';

@Component({
    selector: 'project-section',
    templateUrl: './projects.component.html',
    styles:[require('./projects.component.scss').toString()],
})
export class ProjectsComponent implements OnInit {

    public themeHref: string;
    public projects: any;
    public categories: any;
    public activeCategory: string;

    constructor() {
        let appSetting = new AppSettings();
        this.themeHref = appSetting.APP_THEME_HREF;
        this.projects = window["projects"].data;
        this.getCategories(this.projects);
    }

    ngOnInit() {}

    getCategories(projects: any){
        let categories: any = [];
        for (let project of projects) {
            if(categories.indexOf(project.custome_fields.projects_category.toLowerCase()) === -1){
                categories.push(project.custome_fields.projects_category.toLowerCase());
            }
        }
        this.categories = categories;
    }

    setCategory(category: string, i: number){
        jQuery(".case-study_content_filter_item").removeClass("active");
        if(this.activeCategory === category){
            this.activeCategory = "";
        }else{
            this.activeCategory = category;
            jQuery(jQuery(".case-study_content_filter_item")[i]).addClass("active");
        }

    }
}
