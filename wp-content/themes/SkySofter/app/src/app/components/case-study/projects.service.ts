import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ProjectService {
    public projects: any = [
        {
            sample: "/assets/images/projects/svv-report.jpg",
            name: "Super Volley Report",
            category: "web",
            description: "Rozwiązanie przygotowane jako usługa wsparcia dla operatorów systemu wideoweryfikacji, która odgrywa istotną rolę w realizacji meczy siatkówki. ",
            technologies: [
                {
                    name: "PHP"
                },
                {
                    name: "Zend"
                },
                {
                    name: "Angular"
                },
                {
                    name: "js"
                }
            ]
        },
        {
            sample: "/assets/images/projects/5.png",
            name: "ACCS",
            category: "mobile",
            description: "Jedno ze zwycięskich rozwiązań podczas Microsoft Imagine Cup 2016 w kategorii World Citizenship. ",
            technologies: [
                {
                    name: "Mobile"
                },
                {
                    name: "UI / UX"
                }
            ]
        },
        {
            sample: "/assets/images/projects/6.png",
            name: "Smart Flower",
            category: "mobile",
            description: "Rozwiązanie, dzięki któremu już nigdy nie zapomnisz podlać swoich roślin. ",
            technologies: [
                {
                    name: "Mobile"
                },
                {
                    name: "UI / UX"
                }
            ]
        },
        {
            sample: "/assets/images/projects/meal.png",
            name: "Smart Holder",
            category: "mobile",
            description: "Urządzenie składające się z podstawek ważących o uniwersalnych zastosowaniach. ",
            technologies: [
                {
                    name: "Mobile"
                },
                {
                    name: "UI / UX"
                }
            ]
        },
        {
            sample: "/assets/images/projects/8.png",
            name: "ESA CanSat",
            category: "desktop",
            description: "Zwycięski projekt przygotowany w ramach ogólnopolskiego konkursu Europejskiej Agencji Kosmicznej. ",
            technologies: [
                {
                    name: "Mobile"
                },
                {
                    name: "UI / UX"
                }
            ]
        },
        {
            sample: "/assets/images/projects/svv.jpg",
            name: "IT Sport Tech",
            category: "web",
            description: "IT Sport Tech tworzy elastyczne i wszechstronne narzędzia do optymalizacji arbitrażu w sporcie.",
            technologies: [
                {
                    name: "PHP"
                },
                {
                    name: "xPDO"
                },
                {
                    name: "MODx"
                },
                {
                    name: "js"
                }
            ]
        }
    ];

    constructor(private httpClient: HttpClient){}

    getProjects(): any {
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF+"/wp-json/rest_for_acf/GetFields";
        let headers = new HttpHeaders();
        let params: HttpParams = new HttpParams();
        params = params.set("postType", "projects");
        params = params.set("includeCostumeFields", 'true');
        params = params.set("order", 'DESC');
        params = params.set("custome_fields[0]", 'project_short_description');
        params = params.set("custome_fields[1]", 'projects_category');
        params = params.set("custome_fields[2]", 'technologies');
        return this.httpClient.get(url, { headers, params });
    }
} 