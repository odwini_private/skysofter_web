import { Component } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
export class ProjectsComponent {
    constructor() {
        let appSetting = new AppSettings();
        this.themeHref = appSetting.APP_THEME_HREF;
        this.projects = window["projects"].data;
        this.getCategories(this.projects);
    }
    ngOnInit() { }
    getCategories(projects) {
        let categories = [];
        for (let project of projects) {
            if (categories.indexOf(project.custome_fields.projects_category.toLowerCase()) === -1) {
                categories.push(project.custome_fields.projects_category.toLowerCase());
            }
        }
        this.categories = categories;
    }
    setCategory(category, i) {
        jQuery(".case-study_content_filter_item").removeClass("active");
        if (this.activeCategory === category) {
            this.activeCategory = "";
        }
        else {
            this.activeCategory = category;
            jQuery(jQuery(".case-study_content_filter_item")[i]).addClass("active");
        }
    }
}
ProjectsComponent.decorators = [
    { type: Component, args: [{
                selector: 'project-section',
                templateUrl: './projects.component.html',
                styles: [require('./projects.component.scss').toString()],
            },] },
];
/** @nocollapse */
ProjectsComponent.ctorParameters = () => [];
//# sourceMappingURL=projects.component.js.map