export interface Project {
    sample: string;
    name: string;
    category: string;
    description: string;
    technologies: any;
}
