import { Component, Inject, Input, ElementRef } from '@angular/core';
import { APP_BASE_HREF } from "@angular/common";
import { AppSettings } from '../../../AppSettings';
import { WINDOW } from "../window/window.service";
export class OwlCarouselComponent {
    constructor(baseHref, el, window) {
        this.baseHref = baseHref;
        this.el = el;
        this.window = window;
        this.defaultOptions = {};
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        for (var key in this.options) {
            this.defaultOptions[key] = this.options[key];
        }
        if (this.initWindowSize || this.destroyWindowSize) {
            this.setResizeCarousel();
        }
        this.checkDimensionsAndRemoveOrIntCarousel();
    }
    setResizeCarousel() {
        let that = this;
        $(window).resize(function () {
            clearTimeout(that.reintTimeout);
            that.reintTimeout = setTimeout(function () {
                that.checkDimensionsAndRemoveOrIntCarousel();
                clearTimeout(that.reintTimeout);
            }, 200);
        });
    }
    checkDimensionsAndRemoveOrIntCarousel() {
        if (this.destroyWindowSize) {
            if (this.initWindowSize) {
                if (window.innerWidth <= this.initWindowSize && window.innerWidth > this.destroyWindowSize) {
                    this.initCarousel();
                }
                else {
                    this.destroyCarousel();
                }
            }
            else {
                if (window.innerWidth > this.destroyWindowSize) {
                    this.initCarousel();
                }
                else {
                    this.destroyCarousel();
                }
            }
        }
        else if (this.initWindowSize) {
            if (window.innerWidth <= this.initWindowSize) {
                this.initCarousel();
            }
            else {
                this.destroyCarousel();
            }
        }
        else {
            this.destroyCarousel();
            this.initCarousel();
        }
    }
    destroyCarousel() {
        if (this.$owlElement && this.$owlElement !== undefined && this.$owlElement !== null) {
            this.$owlElement.trigger('destroy.owl.carousel').removeClass('owl-loaded');
            this.$owlElement.find('.owl-stage-outer').children().unwrap();
        }
    }
    initCarousel() {
        if ($(this.el.nativeElement).find(".owl-carousel").children().length > 0) {
            this.$owlElement = $(this.el.nativeElement).find(".owl-carousel");
            this.pinInitEvents(this.$owlElement);
            this.$owlElement = this.$owlElement.owlCarousel(this.defaultOptions);
            this.pinEvents(this.$owlElement);
        }
    }
    pinInitEvents(carousel) {
        let that = this;
        carousel.on('initialized.owl.carousel', function (e) {
            that.setFitstLastClass(this);
        });
    }
    pinEvents(carousel) {
        let that = this;
        // carousel.on('translated.owl.carousel', function (e: any) {
        //     that.setFitstLastClass(this);
        // });
    }
    setFitstLastClass(slider) {
        // $(slider).find(".owl-item").removeClass("first-active");
        // $(slider).find(".owl-item").removeClass("last-active");
        // let activeItems = $(slider).find(".owl-item.active");
        // $(activeItems[0]).addClass("first-active");
        // $(activeItems[activeItems.length - 1]).addClass("last-active");
    }
    ngOnDestroy() {
        this.$owlElement.data('owlCarousel').destroy();
        this.$owlElement = null;
    }
}
OwlCarouselComponent.decorators = [
    { type: Component, args: [{
                selector: 'owl-carousel',
                templateUrl: './owl-carousel.component.html',
                styles: [require('./owl-carousel.component.scss').toString()]
            },] },
];
/** @nocollapse */
OwlCarouselComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [APP_BASE_HREF,] },] },
    { type: ElementRef, },
    { type: Window, decorators: [{ type: Inject, args: [WINDOW,] },] },
];
OwlCarouselComponent.propDecorators = {
    "options": [{ type: Input },],
    "initWindowSize": [{ type: Input },],
    "destroyWindowSize": [{ type: Input },],
};
//# sourceMappingURL=owl-carousel.component.js.map