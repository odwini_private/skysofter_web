import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { OwlCarouselComponent } from './owl-carousel.component';

describe('Owl carousel', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [OwlCarouselComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(OwlCarouselComponent);
        expect(fixture.componentInstance instanceof OwlCarouselComponent).toBe(
            true,
            'should create AppComponent'
        );
    });
});
