import {Component, OnInit, Inject, Input, ElementRef} from '@angular/core';
import {APP_BASE_HREF} from "@angular/common";
import {AppSettings} from '../../../AppSettings';
import {WINDOW} from "../window/window.service";

@Component({
    selector: 'owl-carousel',
    templateUrl: './owl-carousel.component.html',
    styles: [require('./owl-carousel.component.scss').toString()]
})
export class OwlCarouselComponent implements OnInit {
    @Input() options: any;
    @Input() initWindowSize: number;
    @Input() destroyWindowSize: number;
    public appThemeHref: string;
    public defaultOptions: Object = {};
    public reintTimeout: any;
    $owlElement: any;


    constructor(@Inject(APP_BASE_HREF) public baseHref: string, private el: ElementRef, @Inject(WINDOW) private window: Window) {
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        for (var key in this.options) {
            this.defaultOptions[key] = this.options[key];
        }
        if (this.initWindowSize || this.destroyWindowSize) {
            this.setResizeCarousel();
        }
        this.checkDimensionsAndRemoveOrIntCarousel();
    }

    setResizeCarousel() {
        let that = this;
        $(window).resize(function () {
            clearTimeout(that.reintTimeout);
            that.reintTimeout = setTimeout(function(){
                that.checkDimensionsAndRemoveOrIntCarousel();
                clearTimeout(that.reintTimeout);
            }, 200);

        });
    }

    checkDimensionsAndRemoveOrIntCarousel(){
        if(this.destroyWindowSize){
            if(this.initWindowSize){
                if (window.innerWidth <= this.initWindowSize && window.innerWidth > this.destroyWindowSize) {
                    this.initCarousel();
                } else {
                    this.destroyCarousel();
                }
            }else{
                if (window.innerWidth > this.destroyWindowSize) {
                    this.initCarousel();
                } else {
                    this.destroyCarousel();
                }
            }

        }else if(this.initWindowSize){
            if (window.innerWidth <= this.initWindowSize) {
                this.initCarousel();
            } else {
                this.destroyCarousel();
            }
        }else{
            this.destroyCarousel();
            this.initCarousel();
        }
    }

    destroyCarousel() {

        if(this.$owlElement && this.$owlElement !== undefined && this.$owlElement !== null){
            this.$owlElement.trigger('destroy.owl.carousel').removeClass('owl-loaded');
            this.$owlElement.find('.owl-stage-outer').children().unwrap();
        }
    }

    initCarousel() {
        if ($(this.el.nativeElement).find(".owl-carousel").children().length > 0) {
            this.$owlElement = $(this.el.nativeElement).find(".owl-carousel");
            this.pinInitEvents(this.$owlElement);
            this.$owlElement = this.$owlElement.owlCarousel(this.defaultOptions);
            this.pinEvents(this.$owlElement);
        }
    }

    pinInitEvents(carousel: any) {
        let that = this;
        carousel.on('initialized.owl.carousel', function (e: any) {
            that.setFitstLastClass(this);
        });
    }

    pinEvents(carousel: any) {
        let that = this;
        // carousel.on('translated.owl.carousel', function (e: any) {
        //     that.setFitstLastClass(this);
        // });
    }

    setFitstLastClass(slider: any) {
        // $(slider).find(".owl-item").removeClass("first-active");
        // $(slider).find(".owl-item").removeClass("last-active");
        // let activeItems = $(slider).find(".owl-item.active");
        // $(activeItems[0]).addClass("first-active");
        // $(activeItems[activeItems.length - 1]).addClass("last-active");
    }

    ngOnDestroy() {
        this.$owlElement.data('owlCarousel').destroy();
        this.$owlElement = null;
    }
}
