import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { GmapComponent } from './gmap.component';

describe('Gmaps', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [GmapComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(GmapComponent);
        expect(fixture.componentInstance instanceof GmapComponent).toBe(
            true,
            'should create AppComponent'
        );
    });
});
