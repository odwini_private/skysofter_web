export class Particle {
    public shape: any;
    public size: any;
    public defaultSize: any;
    public context: any;
    public circle: any;
    public angle: any;
    public speed: number = 0.0009;
    public curveCoords: any;
    public sides: number;
    public scewAngle: number;
    public Image: any;

    constructor(circle: any) {
        this.shape = this.generateRandomShape();
        this.createAssets(this.shape);
        this.circle = circle;
        this.angle = this.getRandomInt(360);
        this.curveCoords = {
            x: this.circle.center.x + Math.cos(this.angle)*this.circle.naturalSize,
            y: this.circle.center.y + Math.sin(this.angle)*this.circle.naturalSize
        };
        if(this.shape === "drawTreangle"){
            this.sides = 3;
            this.scewAngle = ((Math.PI * 2) / this.sides);
            if(this.size < 10){
                this.size = 10;
                this.defaultSize = 10;
            }
        }

    }

    public createAssets(shape){
        this.defaultSize = this.getRandomInt(10);
        if(this.defaultSize < 6){
            this.defaultSize = 6;
        }
        this.size = this.defaultSize;
        if(shape === "drawTreangle"){
            let treangleSrc = document.getElementById("smallTreangle").getAttribute('src');
            this.Image  = new Image();
            this.Image.src = treangleSrc;
            this.Image.width = this.Image.width * (this.defaultSize/5);
            this.Image.height =  this.Image.height * (this.defaultSize/5);
        }else{
            let circleSrc = document.getElementById("smallCircle").getAttribute('src');
            this.Image  = new Image();
            this.Image.src = circleSrc;
            this.Image.width = this.Image.width * (this.defaultSize/8);
            this.Image.height =  this.Image.height * (this.defaultSize/8);
        }


    }

    public draw(){
        if(this.angle >= 360){
            this.angle = 0;
        }
        this.angle = this.angle + this.speed;
        if(this.shape === "drawTreangle"){
            this.curveCoords = {
                x: this.circle.center.x + Math.cos(this.angle)*this.circle.naturalSize + this.Image.width/2,
                y: this.circle.center.y + Math.sin(this.angle)*this.circle.naturalSize + this.Image.height/2
            };
        }else{
            this.curveCoords = {
                x: this.circle.center.x + Math.cos(this.angle)*this.circle.naturalSize,
                y: this.circle.center.y + Math.sin(this.angle)*this.circle.naturalSize + this.Image.height/2
            };
        }

        this[this.shape]();
    }

    private generateRandomShape() {
        let func = "drawCircle";
        let drawFunctions = [
          "drawCircle",
          "drawCircle",
          "drawTreangle",
          "drawTreangle",
          "drawCircle"
        ];
        func = drawFunctions[this.getRandomInt(5)];
        return func;
    }

    public drawCircle() {
        let context = this.circle.particlesContext;
        context.drawImage(this.Image, this.curveCoords.x - (this.Image.width/2), this.curveCoords.y - (this.Image.height), this.Image.width, this.Image.height);
    }

    public setShadow(color: string, ox: number, oy: number, blur: number){
        let context = this.circle.particlesContext;
        context.shadowColor = color;
        context.shadowOffsetX = ox;
        context.shadowOffsetY = oy;
        context.shadowBlur = blur;
    }

    public drawRectangle() {
        return this.size;
    }

    public drawTreangle() {
        let context = this.circle.particlesContext;
        context.drawImage(this.Image, this.curveCoords.x - (this.Image.width), this.curveCoords.y - (this.Image.height), this.Image.width, this.Image.height);
    }

    private getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    public resetCanvas(){

    }

    public defineEvents(){

    }
}