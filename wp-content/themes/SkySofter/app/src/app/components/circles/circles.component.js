import { Component, Inject, Input, ElementRef } from '@angular/core';
import { APP_BASE_HREF } from "@angular/common";
import { AppSettings } from '../../../AppSettings';
import { CirclesCanvas } from './circles-animation';
import { merge } from 'lodash';
export class CirclesComponent {
    constructor(baseHref, el) {
        this.baseHref = baseHref;
        this.el = el;
        this.defaultOptions = {
            circles: 5,
            min_size: 5,
            max_size: 100
        };
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        $(document).ready(function () {
            this.ciclesCanvas = new CirclesCanvas(this.el, this._options);
        });
    }
    set options(options) {
        this._options = merge(this.defaultOptions, options);
    }
}
CirclesComponent.decorators = [
    { type: Component, args: [{
                selector: 'circles',
                templateUrl: './circles.component.html',
                styles: [require('./circles.component.scss').toString()]
            },] },
];
/** @nocollapse */
CirclesComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [APP_BASE_HREF,] },] },
    { type: ElementRef, },
];
CirclesComponent.propDecorators = {
    "options": [{ type: Input },],
};
//# sourceMappingURL=circles.component.js.map