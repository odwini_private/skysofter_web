import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { CirclesComponent } from './circles.component';

describe('Circles', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [CirclesComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(CirclesComponent);
        expect(fixture.componentInstance instanceof CirclesComponent).toBe(
            true,
            'should create AppComponent'
        );
    });
});
