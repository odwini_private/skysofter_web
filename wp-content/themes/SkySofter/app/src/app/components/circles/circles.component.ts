import { Component, OnInit, Inject, Input, ElementRef } from '@angular/core';
import { APP_BASE_HREF } from "@angular/common";
import {AppSettings} from '../../../AppSettings';
import {CirclesCanvas} from './circles-animation';
import { isEmpty, merge } from 'lodash';

@Component({
    selector: 'circles',
    templateUrl: './circles.component.html',
    styles:[require('./circles.component.scss').toString()]
})
export class CirclesComponent implements OnInit {
    public appThemeHref: string;
    public ciclesCanvas: any;
    public _options: Object;
    private defaultOptions: Object ={
        circles: 5,
        min_size: 5,
        max_size: 100
    };

    constructor(@Inject(APP_BASE_HREF) public baseHref: string, private el: ElementRef) {
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
    }

    ngOnInit() {

    }

    ngAfterViewInit(){
        $(document).ready(function(){
            this.ciclesCanvas = new CirclesCanvas(this.el, this._options);
        });
    }

    @Input()
    set options(options: Object) {
        this._options = merge(this.defaultOptions, options );
    }


}
