import {Particle} from './particle';
import {isEmpty} from 'lodash'
export class Circle {
    public size: number;
    public naturalSize: number;
    public defaultSize: number;
    private context: any;
    private canvas: any;
    private particlesCanvas: any;
    private particlesContext: any;
    public center: any;
    public particlesNum: number;
    public particles: any;
    public sizeChar: number = 1;
    public options: any;

    constructor(options: any, canvas: any, context: any, particles: number, middleSize: number, particlesCanvas: any, particlesContext: any) {
        this.context = context;
        this.canvas = canvas;
        this.particlesCanvas = particlesCanvas;
        this.particlesContext = particlesContext;
        this.particlesNum = particles;
        this.defaultSize = middleSize;
        this.options = options;
        this.size = middleSize;
        this.drawCircles();
        return this;
    }

    public drawCircles() {
        this.center = {
            x: this.canvas.width / 2,
            y: this.canvas.height / 2
        };
        this.setSize(0.01);
        this.createParticles();
        this.setSize(0.01);
        this.context.beginPath();
        this.context.arc(this.center.x, this.center.y, this.naturalSize, 0, 2 * Math.PI);
        if (this.options.circlesColor) {
            this.context.strokeStyle = this.options.circlesColor;
        } else {
            this.context.strokeStyle = "#f2f2ff";
        }
        this.context.lineWidth = 2;
        this.context.stroke();
        this.draw();
    }

    public draw() {
        if (!isEmpty(this.particles)) {
            for (let particle = 0; Object.keys(this.particles).length > particle; particle++) {
                this.particles[Object.keys(this.particles)[particle]].draw();
            }
        }
    }

    public setSize(sizeImpact) {

        if (this.size - this.defaultSize > 4 || this.size - this.defaultSize < -4) {
            this.sizeChar = this.sizeChar * -1;
        }
        this.size = this.size + (sizeImpact * this.sizeChar);
        let size = this.canvas.width / 100 * this.size;
        this.naturalSize = size;
    }

    public createParticles() {
        this.particles = {};
        for (let particle = 0; this.particlesNum > particle; particle++) {
            this.particles[Object.keys(this.particles).length] = new Particle(this);
        }
    }
}