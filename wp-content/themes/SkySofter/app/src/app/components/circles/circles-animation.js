import { Circle } from './circle';
export class CirclesCanvas {
    constructor(element, options) {
        this.fpsInterval = 1000 / 60;
        this.then = Date.now();
        this.startTime = this.then;
        this.options = options;
        this.CreateCanvas(element);
        this.createOrbits();
        this.draw();
        return this;
    }
    CreateCanvas(element) {
        this.parentElement = element.nativeElement;
        this.canvas = document.createElement('canvas');
        this.particlesCanvas = document.createElement('canvas');
        this.defineCanvasSize();
        this.parentElement.appendChild(this.canvas);
        this.parentElement.appendChild(this.particlesCanvas);
        this.context = this.canvas.getContext("2d");
        this.particlesContext = this.particlesCanvas.getContext("2d");
        this.defineResize();
    }
    createOrbits() {
        let orbitsSize = this.defineSizesOfOrbits();
        this.orbits = {};
        for (let circleIndex = 0; this.options.circles > circleIndex; circleIndex++) {
            this.orbits[Object.keys(this.orbits).length] =
                new Circle(this.options, this.canvas, this.context, circleIndex, orbitsSize[circleIndex], this.particlesCanvas, this.particlesContext);
        }
    }
    defineSizesOfOrbits() {
        let sizes = [];
        let minSize = this.options.min_size;
        let maxSize = this.options.max_size;
        let middleCircles = this.options.circles - 1;
        let oneStepSize = (maxSize - minSize) / middleCircles;
        sizes.push(minSize);
        for (let circle = 0; middleCircles > circle; circle++) {
            sizes.push(sizes[sizes.length - 1] + oneStepSize);
        }
        return sizes;
    }
    defineResize() {
        let that = this;
        $(window).resize(function () {
            clearTimeout(that.resizeTimeout);
            that.resizeTimeout = setTimeout(function () {
                that.defineCanvasSize();
                for (let circleIndex = 0; that.options.circles > circleIndex; circleIndex++) {
                    that.orbits[circleIndex].drawCircles();
                }
                clearTimeout(that.resizeTimeout);
            }, 300);
        });
    }
    forceRedrawCanvas() {
        this.defineCanvasSize();
        for (let circleIndex = 0; this.options.circles > circleIndex; circleIndex++) {
            this.orbits[circleIndex].drawCircles();
        }
        this.draw();
    }
    forceStopDrawing() {
        cancelAnimationFrame(this.frameId);
    }
    defineCanvasSize() {
        let width = this.parentElement.parentElement.offsetWidth;
        let height = this.parentElement.parentElement.offsetHeight;
        this.canvas.width = width;
        this.particlesCanvas.width = width;
        this.canvas.height = height;
        this.particlesCanvas.height = height;
    }
    draw() {
        let that = this;
        this.frameId = window.requestAnimationFrame(function () {
            // let  now = Date.now();
            // let elapsed = now - that.then;
            // if (elapsed > that.fpsInterval) {
            //     that.then = now - (elapsed % that.fpsInterval);
            that.particlesContext.clearRect(0, 0, that.canvas.width, that.canvas.height);
            for (let particle = 0; Object.keys(that.orbits).length > particle; particle++) {
                that.orbits[Object.keys(that.orbits)[particle]].draw();
            }
            // }
            that.draw();
        });
    }
}
//# sourceMappingURL=circles-animation.js.map