import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
export class ClientsPartnersService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.cleintspartners = [
            {
                // altlogo: "/assets/images/clients/legal_care-gray.png",
                logo: "/assets/images/clients/legal_care.png",
                title: "Lorem ipsum dolor sit amet",
                url: "http://legal-care.pl"
            },
            {
                // altlogo: "/assets/images/clients/nasa-gray.png",
                logo: "/assets/images/clients/nasa.png",
                title: "Lorem ipsum dolor sit amet",
                url: "https://www.nasa.gov/"
            },
            {
                // altlogo: "/assets/images/clients/itsport_tech-gray.png",
                logo: "/assets/images/clients/itsport_tech.png",
                title: "Lorem ipsum dolor sit amet",
                url: "https://itsport.tech/"
            },
            {
                // altlogo: "/assets/images/clients/invig-gray.png",
                logo: "/assets/images/clients/invig.png",
                title: "Lorem ipsum dolor sit amet",
                url: "http://invig.pl/"
            },
            {
                // altlogo: "/assets/images/clients/invig-gray.png",
                logo: "/assets/images/clients/logo-vlp-trsansparent.png",
                title: "Lorem ipsum dolor sit amet",
                url: "http://vlp.pl"
            }
        ];
    }
    getServices() {
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF + "/wp-json/rest_for_acf/GetFields";
        let headers = new HttpHeaders();
        let params = new HttpParams();
        params = params.set("postType", "cliets_and_partners");
        params = params.set("fields[0]", 'url');
        return this.httpClient.get(url, { headers, params });
    }
}
ClientsPartnersService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ClientsPartnersService.ctorParameters = () => [
    { type: HttpClient, },
];
//# sourceMappingURL=client-partners.service.js.map