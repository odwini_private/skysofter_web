import { Input, Directive, ElementRef } from '@angular/core';
export class AltImage {
    constructor(_elementRef) {
        this._elementRef = _elementRef;
    }
}
AltImage.decorators = [
    { type: Directive, args: [{
                selector: '[alt-image]'
            },] },
];
// @HostListener('mouseover') mouseover() {
//     let altSrc = this.imageSrc;
//     let currentSrc = this._elementRef.nativeElement.children[0].getAttribute('src');
//     this._elementRef.nativeElement.children[0].setAttribute('src', altSrc);
//     this.imageSrc = currentSrc;
// }
//
// @HostListener('mouseout') mouseout() {
//     let altSrc = this.imageSrc;
//     let currentSrc = this._elementRef.nativeElement.children[0].getAttribute('src');
//     this._elementRef.nativeElement.children[0].setAttribute('src', altSrc);
//     this.imageSrc = currentSrc;
// }
/** @nocollapse */
AltImage.ctorParameters = () => [
    { type: ElementRef, },
];
AltImage.propDecorators = {
    "imageSrc": [{ type: Input, args: ['alt-image',] },],
};
//# sourceMappingURL=clients-partners.logo.directive.js.map