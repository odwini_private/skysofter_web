import {} from 'jasmine';
import { TestBed } from '@angular/core/testing';

import { ClientsPartnersComponent } from './clients-partners.component';

describe('ClientsParters', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [ClientsPartnersComponent] });
    });

    it('should work', () => {
        let fixture = TestBed.createComponent(ClientsPartnersComponent);
        expect(fixture.componentInstance instanceof ClientsPartnersComponent).toBe(
            true,
            'should create AppComponent'
        );
    });
});
