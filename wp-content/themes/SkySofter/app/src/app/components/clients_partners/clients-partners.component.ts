import {Component, OnInit, Inject, ViewChild} from '@angular/core';
import {APP_BASE_HREF} from "@angular/common";
import  {AppSettings} from '../../../AppSettings';
import {ClientsPartnersService} from './client-partners.service';
import {OwlCarouselComponent} from '../owl-carousel/owl-carousel.component';


@Component({
    selector: 'clients-partners',
    templateUrl: './clients-partners.component.html',
    styles: [require('./clients-partners.component.scss').toString()],
    providers: [ClientsPartnersService]
})
export class ClientsPartnersComponent implements OnInit {
    @ViewChild(OwlCarouselComponent) owlCarouselComponent: OwlCarouselComponent;
    public appThemeHref: string;
    public carouselOptions: Object;
    public clientspartners: any;
    public destroyWindowSize: number;

    constructor(@Inject(APP_BASE_HREF) public baseHref: string, public _ClientsPartnersService: ClientsPartnersService) {
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
        this.clientspartners = window["clients_partners"].data;
        this.destroyWindowSize = 650;
        this.carouselOptions = {
            navigation: false,
            pagination: false,
            loop: false,
            smartSpeed: 1200,
            fluidSpeed: 500,
            rewindNav: false,
            margin: 25,
            items: 1,
            responsive: {
                750: {
                    items: 2
                },
                950: {
                    items: 3
                },
                1250: {
                    items: 4
                },
                1560: {
                    items: 5
                }
            }
        };
    }

    ngOnInit() {
    }

}
