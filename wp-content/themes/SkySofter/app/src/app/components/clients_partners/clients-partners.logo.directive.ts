import { Input, Directive, HostListener, ElementRef } from '@angular/core';
@Directive({
    selector: '[alt-image]'
})
export class AltImage {
    @Input('alt-image') imageSrc: string;

    constructor(private _elementRef: ElementRef) {}

    // @HostListener('mouseover') mouseover() {
    //     let altSrc = this.imageSrc;
    //     let currentSrc = this._elementRef.nativeElement.children[0].getAttribute('src');
    //     this._elementRef.nativeElement.children[0].setAttribute('src', altSrc);
    //     this.imageSrc = currentSrc;
    // }
    //
    // @HostListener('mouseout') mouseout() {
    //     let altSrc = this.imageSrc;
    //     let currentSrc = this._elementRef.nativeElement.children[0].getAttribute('src');
    //     this._elementRef.nativeElement.children[0].setAttribute('src', altSrc);
    //     this.imageSrc = currentSrc;
    // }
}