import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class NavService {
    public news: any;

    constructor(private httpClient: HttpClient){}

    getNav(): any {
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF+"/wp-json/rest_for_acf/GetMenu";
        let headers = new HttpHeaders();
        let params: HttpParams = new HttpParams();
        params = params.set("navMenu", "Top nav");
        return this.httpClient.get(url, { headers, params });
    }

} 