import { Injectable } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
export class NavService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getNav() {
        let appSetting = new AppSettings();
        let url = appSetting.APP_HREF + "/wp-json/rest_for_acf/GetMenu";
        let headers = new HttpHeaders();
        let params = new HttpParams();
        params = params.set("navMenu", "Top nav");
        return this.httpClient.get(url, { headers, params });
    }
}
NavService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
NavService.ctorParameters = () => [
    { type: HttpClient, },
];
//# sourceMappingURL=nav.service.js.map