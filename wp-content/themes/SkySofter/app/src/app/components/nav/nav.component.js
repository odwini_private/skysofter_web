import { Component, HostListener, Inject } from '@angular/core';
import { AppSettings } from '../../../AppSettings';
import { DOCUMENT } from "@angular/platform-browser";
import * as ScrollTo from "./pagescroller";
import { WINDOW } from "../window/window.service";
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
export class NavComponent {
    constructor(document, window) {
        this.document = document;
        this.window = window;
        let appSetting = new AppSettings();
        this.themeHref = appSetting.APP_THEME_HREF;
        if (window.innerWidth <= 1250) {
            this.navFixed = false;
        }
        else {
            this.navFixed = false;
        }
        this.navItems = window["main_nav"].data;
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        $(".nav-burger").click(function () {
            $(".mobile_nav").fadeIn(300);
            disableBodyScroll($(".mobile_nav")[0]);
        });
        let menuScroller = new ScrollTo.ScrollTo({
            swingSpeedPerHeightUnit: 200,
            heightFix: 0
        });
        $(".nav-item, .nav-button, .close_menu").click(function () {
            $(".mobile_nav").fadeOut(300);
            enableBodyScroll($(".mobile_nav")[0]);
        });
        this.lastScrollPosition = window.pageYOffset;
        if (window.pageYOffset > 10) {
            this.navScrolled = true;
        }
        else {
            this.navScrolled = false;
            this.navFixed = false;
        }
    }
    onWindowResize() {
        let that = this;
        if (window.innerWidth <= 1250) {
            that.navFixed = false;
        }
    }
    onWindowScroll(e) {
        let that = this;
        let number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
        if (window.innerWidth > 1250) {
            if (number > 100) {
                this.navFixed = true;
            }
            else if (this.navFixed && number < 10) {
                this.navFixed = false;
            }
        }
        else {
            this.mobileMenuScrollBack();
        }
    }
    mobileMenuScrollBack() {
        if (this.lastScrollPosition > (window.pageYOffset + 25) && this.navFixed === false) {
            this.lastScrollPosition = window.pageYOffset;
            this.navFixed = true;
            $(".nav").stop().fadeOut(function () {
                $(".nav").css("position", "fixed");
                $(".nav").fadeIn();
            });
        }
        else if (this.lastScrollPosition < (window.pageYOffset - 25)) {
            this.navFixed = false;
            this.lastScrollPosition = window.pageYOffset;
            $(".nav").fadeIn(function () {
                $(".nav").css("position", "absolute");
                $(".nav").fadeOut();
            });
        }
        else {
            this.lastScrollPosition = window.pageYOffset;
        }
        if (window.pageYOffset > 10) {
            this.navScrolled = true;
        }
        else {
            this.navScrolled = false;
            this.navFixed = false;
            $(".nav").css("position", "absolute");
            $(".nav").stop().fadeIn();
        }
    }
}
NavComponent.decorators = [
    { type: Component, args: [{
                selector: 'nav-section',
                templateUrl: './nav.component.html',
                styles: [require('./nav.component.scss').toString()],
            },] },
];
/** @nocollapse */
NavComponent.ctorParameters = () => [
    { type: Document, decorators: [{ type: Inject, args: [DOCUMENT,] },] },
    { type: Window, decorators: [{ type: Inject, args: [WINDOW,] },] },
];
NavComponent.propDecorators = {
    "onWindowResize": [{ type: HostListener, args: ["window:resize", [],] },],
    "onWindowScroll": [{ type: HostListener, args: ["window:scroll", [],] },],
};
//# sourceMappingURL=nav.component.js.map