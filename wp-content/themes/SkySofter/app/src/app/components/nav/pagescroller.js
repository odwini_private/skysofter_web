export class ScrollTo {
    constructor(swingParams) {
        this.setParams(swingParams);
        this.setScrollElements();
        this.bindEvents();
        this.resizeFix();
        this.checkLinkForScrollProperty();
        return this;
    }

    setParams(swingParams) {
        this.params = {
            swingSpeedPerHeightUnit: 500,
            swingHeighUnit: 620
        };
        if (swingParams) {
            this.overrideDefault(swingParams);
        }
    }

    setScrollElements() {
        let that = this;
        let scrollElmenets = this.scrollElmenets = {};
        $("*[data-scrollto]").each(function () {
            let navElement = $(this);
            navElement.url = $(this).attr("href");
            navElement.elementLink = navElement.attr("data-scrollto");
            let elementSelector = navElement.elementLink;
            let scrollToElement = $(elementSelector);
            let scrollToElementOffset = "";
            if (scrollToElement.length > 0) {
                scrollToElementOffset = scrollToElement.offset().top - parseInt(scrollToElement.css("padding-top"));
                let heightFix = that.params.heightFix;
                if(heightFix){
                    scrollToElementOffset = scrollToElement.offset().top - parseInt(scrollToElement.css("padding-top")) + heightFix;
                }
            } else {
                scrollToElementOffset = 0;
            }
            if (scrollElmenets[elementSelector]) {
                scrollElmenets[elementSelector].scrollToElement = scrollToElement;
                if(scrollElmenets[elementSelector].navElement.length === 0){
                    scrollElmenets[elementSelector].navElement[0] = navElement;
                }else{
                    scrollElmenets[elementSelector].navElement.push(navElement[0]);
                }
                scrollElmenets[elementSelector].offsetTop = scrollToElementOffset;
            } else {
                scrollElmenets[elementSelector] = {
                    navElement: navElement,
                    scrollToElement: scrollToElement,
                    offsetTop: scrollToElementOffset
                };
            }

        });
        return scrollElmenets;
    }

    getBlockSelectorFromLink(link) {
        link = this.getHashFromLink(link);
        let hashAssocArray = this.locationHashIntoAssoc(link);
        let blockSelector = hashAssocArray.scrollto;
        return blockSelector;
    }

    overrideDefault(params) {
        let that = this;
        let keys = Object.keys(params);
        keys.forEach(function (element) {
            that.params[element] = params[element];
        });
    }

    bindEvents() {
        let that = this;
        let scrollElements = this.scrollElmenets;
        let ScrollElementsKeys = Object.keys(scrollElements);
        ScrollElementsKeys.forEach(function(ScrollElementKey){
            let scrollElement = scrollElements[ScrollElementKey];
            that.bindScroll(scrollElement, "click");
        });
        this.watchToScrollPosition();
    }

    bindScroll(element, event) {
        let that = this;
        let navElement = element.navElement;
        let eventFunction = function (e) {
            e.preventDefault();
            e.stopPropagation();
            that.scrollToElement(element);
        };
        eventFunction.bind(that);
        if (navElement.length > 1) {
            $(navElement).each(function (element) {
                $(this).unbind(event, eventFunction);
                $(this).on(event, eventFunction);
            });
        } else {
            navElement.unbind(event, eventFunction);
            navElement.on(event, eventFunction);
        }

    }

    scrollToElement(element) {
        if (typeof element === "string") {
            element = this.scrollElmenets[element];
        }
        let currentPage = window.location.origin + window.location.pathname;
        if(element.navElement.url !== "#" && element.navElement.url !== undefined){
            if(element.scrollToElement.length === 0
                && ( element.navElement.url !== currentPage
                && element.navElement.url+"/" !== currentPage)  ){
                this.setLocation(element);
            }
        }

        if(element.scrollToElement.length === 0){
            return false;
        }
        let offsetTop = this.checkElementTop(element);
        let currentTopPosition = document.documentElement.scrollTop || document.body.scrollTop;
        let scrollDistance = Math.abs(offsetTop - currentTopPosition);
        let swingParams = this.params;
        let scrollSpeed = swingParams.swingSpeedPerHeightUnit * (scrollDistance / swingParams.swingHeighUnit);
        let body = $("body, html");
        body.stop().animate({scrollTop: offsetTop}, scrollSpeed, "swing", function () {
            window.localStorage.scrollTo = undefined;
        });
    }

    checkElementTop(element) {

        if ($(element.scrollToElement).parent().attr("data-scrolltop")) {
            let offsetTop = $(element.scrollToElement).parent().attr("data-scrolltop");
        } else if (element.scrollToElement.length > 0) {
            let offsetTop = element.scrollToElement.offset().top - parseInt(element.scrollToElement.css("padding-top"));
            if(this.params.heightFix){
                offsetTop = element.scrollToElement.offset().top - parseInt(element.scrollToElement.css("padding-top")) + this.params.heightFix;
            }
            element.offsetTop = offsetTop;
            if(element.scrollToElement.hasClass("partners_section")){
                element.offsetTop += 80;
            }
        } else {
            element.offsetTop = 0;
        }
        let offsetTop = element.offsetTop;
        return offsetTop;
    }

    watchToScrollPosition() {
        let that = this;
        $(window).scroll(function (e) {
            let topPos = $(window).scrollTop();
            let element = that.closestElementOffsetAndRemoveClass(topPos, that.scrollElmenets, "offsetTop");
            if (element && element.scrollToElement.length > 0) {
                if(element.navElement.length > 1){
                    $(element.navElement).each(function (element) {
                        $(this).addClass("active");
                    });
                }else{
                    element.navElement.addClass("active");
                }

            } else {
                let elementKeys = Object.keys(that.scrollElmenets);
                for (let elementKey of elementKeys) {
                    if (that.scrollElmenets[elementKey].navElement.length > 1) {
                        $(that.scrollElmenets[elementKey].navElement).each(function (element) {
                            $(this).removeClass("active");
                        });
                    } else {
                        that.scrollElmenets[elementKey].navElement.removeClass("active");
                    }
                }
            }
        });
    }

    closestElementOffsetAndRemoveClass(num, arr, key) {

        let arrKeys = Object.keys(arr);
        let current = arr[arrKeys[0]];
        let diff = Math.abs(num - current[key]);
        if (num >= current[key]) {
            for (let arrKey of arrKeys) {
                if (arr[arrKey].navElement.length > 1) {
                    $(arr[arrKey].navElement).each(function (element) {
                        $(this).removeClass("active");
                    });
                } else {
                    arr[arrKey].navElement.removeClass("active");
                }
                let newdiff = Math.abs(num - arr[arrKey][key]);
                if (newdiff < diff) {
                    diff = newdiff;
                    current = arr[arrKey];
                }
            }
        } else {
            current = false;
        }
        return current;
    }

    resizeFix() {
        let resizeTimeout;
        let that = this;
        $(window).resize(function () {
            clearTimeout(resizeTimeout);
            resizeTimeout = setTimeout(function () {
                that.setScrollElements();
                clearTimeout(resizeTimeout);
            }, 800);
        });
    }

    locationHashIntoAssoc(link) {
        let form_data = {};
        $.each(link.replace("#", "").split("&"), function (i, value) {
            value = value.split("=");
            if(value[0] !== "" && value[0] !== undefined && value[0] !== null){
                form_data[value[0]] = value[1];
            }
        });
        return form_data;
    }

    getHashFromLink(link) {
        let hash = "";
        if (link) {
            hash = link.substr(link.indexOf("#"));
        }
        return hash;
    }

    setLocation(element) {
        window.localStorage.setItem('scrollTo', element.navElement.elementLink);
        window.location = element.navElement.url;
    }

    checkLinkForScrollProperty() {
        let that = this;
        let currentHash = window.location.hash;
        let assocHash = this.locationHashIntoAssoc(currentHash);
        let localStorageScrollTo = window.localStorage.scrollTo;
        if (assocHash.scrollto && Object.keys(assocHash).length > 0) {
            $(document).ready(function () {
                that.scrollToElement(assocHash.scrollto);
            });
        }else if(localStorageScrollTo
            && localStorageScrollTo !== undefined
            && localStorageScrollTo !== 'undefined'
            && localStorageScrollTo !== null
            && localStorageScrollTo !== ""){
            assocHash.scrollto = localStorageScrollTo;
            $(document).ready(function () {
                that.scrollToElement(assocHash.scrollto);
            });
        }
    }

}
