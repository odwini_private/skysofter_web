import { Component, Inject } from '@angular/core';
import { APP_BASE_HREF } from "@angular/common";
import { AppSettings } from '../AppSettings';
export class AppComponent {
    constructor(baseHref) {
        this.baseHref = baseHref;
        let appSetting = new AppSettings();
        this.appThemeHref = appSetting.APP_THEME_HREF;
    }
    ngOnInit() {
    }
}
AppComponent.decorators = [
    { type: Component, args: [{
                selector: 'my-app',
                templateUrl: './app.component.html',
                styles: [
                    require('./common.scss').toString(),
                    require('./app.component.scss').toString()
                ]
            },] },
];
/** @nocollapse */
AppComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [APP_BASE_HREF,] },] },
];
//# sourceMappingURL=app.component.js.map