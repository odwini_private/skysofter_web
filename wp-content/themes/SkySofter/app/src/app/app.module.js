import { CommonModule, APP_BASE_HREF, registerLocaleData } from '@angular/common';
import { NgModule, enableProdMode, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import localePl from '@angular/common/locales/pl';
import { WINDOW_PROVIDERS } from "./components/window/window.service";
import { AgmCoreModule } from '@agm/core';
registerLocaleData(localePl);
import { AppComponent } from './app.component';
import { OwlCarouselComponent } from './components/owl-carousel/owl-carousel.component';
import { NavComponent } from "./components/nav/nav.component";
import { HeaderComponent } from './components/header/header.component';
import { ServicesComponent } from './components/services/services.component';
import { TeamComponent } from './components/team/team.component';
import { ProjectsComponent } from './components/case-study/projects.component';
import { NewsComponent } from './components/news/news.component';
import { ClientsPartnersComponent } from './components/clients_partners/clients-partners.component';
import { AltImage } from './components/clients_partners/clients-partners.logo.directive';
import { ContactComponent } from './components/contact/contact.component';
import { CirclesComponent } from './components/circles/circles.component';
import { GetAnEstimateComponent } from './components/get_an_estimate/get-an-estimate.component';
import { GmapComponent } from './components/google-maps/gmap.component';
import { ProjectsPipe } from './components/case-study/projects.pipe';
import { ObjectPairsPipe } from './filters/object-pairs/object-pairs.pipe';
import { DateToSecPipe } from './filters/date-to-sec/date-to-sec.pipe';
if (process.env.ENV === "production") {
    enableProdMode();
}
export class AppModule {
    constructor() {
    }
}
AppModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    AppComponent,
                    OwlCarouselComponent,
                    NavComponent,
                    HeaderComponent,
                    ServicesComponent,
                    TeamComponent,
                    ProjectsComponent,
                    ProjectsPipe,
                    NewsComponent,
                    ClientsPartnersComponent,
                    AltImage,
                    ContactComponent,
                    CirclesComponent,
                    GetAnEstimateComponent,
                    GmapComponent,
                    ObjectPairsPipe,
                    DateToSecPipe
                ],
                imports: [
                    CommonModule,
                    BrowserModule,
                    FormsModule,
                    ReactiveFormsModule,
                    HttpClientModule,
                    AgmCoreModule.forRoot({
                        apiKey: 'AIzaSyD0WsytH3OZ1wKUXGy1p5Vrsa0n0bBORe4'
                    })
                ],
                exports: [],
                providers: [
                    { provide: APP_BASE_HREF, useValue: 'http://localhost/SkySofter/' },
                    { provide: LOCALE_ID, useValue: "pl" },
                    WINDOW_PROVIDERS
                ],
                bootstrap: [AppComponent]
            },] },
];
/** @nocollapse */
AppModule.ctorParameters = () => [];
//# sourceMappingURL=app.module.js.map