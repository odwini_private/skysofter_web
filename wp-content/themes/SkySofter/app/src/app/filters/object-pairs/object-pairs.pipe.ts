import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'objectPairs'
})
export class ObjectPairsPipe implements PipeTransform {
    transform(items: Object[], pairs: number): Object[] {
        if(!items) return [];
        let pairedObject = [];
        let pairsNumber = Math.ceil(Object.keys(items).length / pairs);
        let lastItemInPair = 0;
        for(let i = 0; i < pairsNumber; i++){
            pairedObject[i] = {
                0: items[lastItemInPair],
                1: items[lastItemInPair++]
            }
        }
       return pairedObject;
    }
}