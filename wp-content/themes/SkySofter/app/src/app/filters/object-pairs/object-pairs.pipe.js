import { Pipe } from '@angular/core';
export class ObjectPairsPipe {
    transform(items, pairs) {
        if (!items)
            return [];
        let pairedObject = [];
        let pairsNumber = Math.ceil(Object.keys(items).length / pairs);
        let lastItemInPair = 0;
        for (let i = 0; i < pairsNumber; i++) {
            pairedObject[i] = {
                0: items[lastItemInPair],
                1: items[lastItemInPair++]
            };
        }
        return pairedObject;
    }
}
ObjectPairsPipe.decorators = [
    { type: Pipe, args: [{
                name: 'objectPairs'
            },] },
];
//# sourceMappingURL=object-pairs.pipe.js.map