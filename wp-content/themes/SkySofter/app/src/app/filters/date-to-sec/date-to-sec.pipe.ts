import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'DateToSec'
})
export class DateToSecPipe implements PipeTransform {
    transform(date: any): any {
        if(!date) return "";
        let t = new Date(1970, 0, 1);
        t.setSeconds(date);
        return t;
    }
}