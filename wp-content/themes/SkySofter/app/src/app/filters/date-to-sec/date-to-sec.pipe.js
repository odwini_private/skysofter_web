import { Pipe } from '@angular/core';
export class DateToSecPipe {
    transform(date) {
        if (!date)
            return "";
        let t = new Date(1970, 0, 1);
        t.setSeconds(date);
        return t;
    }
}
DateToSecPipe.decorators = [
    { type: Pipe, args: [{
                name: 'DateToSec'
            },] },
];
//# sourceMappingURL=date-to-sec.pipe.js.map