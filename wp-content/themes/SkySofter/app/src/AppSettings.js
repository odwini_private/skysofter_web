export class AppSettings {
    constructor() {
        this.APP_THEME_HREF = 'http://localhost/SkySofter/wp-content/themes/SkySofter';
        this.APP_HREF = 'http://localhost/SkySofter';
        if (process.env.ENV === "dev") {
            this.APP_THEME_HREF = 'http://localhost/SkySofter/wp-content/themes/SkySofter';
        }
        else {
            this.APP_THEME_HREF = 'https://skysofter.com/wp-content/themes/SkySofter';
        }
        if (process.env.ENV === "dev") {
            this.APP_HREF = 'http://localhost/SkySofter';
        }
        else {
            this.APP_HREF = 'https://skysofter.com';
        }
    }
}
//# sourceMappingURL=AppSettings.js.map