import { enableProdMode } from '@angular/core';
import { platformBrowser } from '@angular/platform-browser';
import * as $ from "jquery";
window["$"] = $;
import { AppModuleNgFactory } from '../aot/src/app/app.module.ngfactory';
enableProdMode();
platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);
//# sourceMappingURL=main.aot.js.map