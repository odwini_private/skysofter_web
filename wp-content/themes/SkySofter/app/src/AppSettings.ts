export class AppSettings {
    public APP_THEME_HREF: string = 'http://localhost/SkySofter/wp-content/themes/SkySofter' ;
    public APP_HREF: string = 'http://localhost/SkySofter' ;
    constructor(){
        if(process.env.ENV === "dev"){
            this.APP_THEME_HREF='http://localhost/SkySofter/wp-content/themes/SkySofter';
        }else{
            this.APP_THEME_HREF='https://skysofter.com/wp-content/themes/SkySofter';
        }
        if(process.env.ENV === "dev"){
            this.APP_HREF='http://localhost/SkySofter';
        }else{
            this.APP_HREF='https://skysofter.com';
        }
    }
}