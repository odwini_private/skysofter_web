<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package SkySofter
 * @subpackage Sky_Softer
 * @since Sky Softer 1.0
 */
?>

<?php wp_footer(); ?>
</body>
</html>
