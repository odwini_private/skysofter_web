var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var helpers = require('./helpers');
var isProd = process.env.NODE_ENV === 'production';
var CopyWebpackPlugin = require('copy-webpack-plugin');
var autoprefixer = require('autoprefixer');
// var MiniCssExtractPlugin = require("mini-css-extract-plugin");

const extractSass = new ExtractTextPlugin({
    filename: "[name].css"
});
const extractCSS = new ExtractTextPlugin({filename: "assets/css/vendor.css"});

module.exports = {
    entry: {
        polyfills: './app/src/polyfills.ts',
        vendor: './app/src/vendor.ts',
        app: './app/src/main.ts',
        css: './app/src/styles'
    },
    resolve: {
        extensions: ['.ts', '.js']
    },

    module: {
        rules: [{
            test: /\.ts$/,
            loaders: [
                'babel-loader',
                {
                    loader: 'awesome-typescript-loader',
                    options: {
                        configFileName: isProd ?
                            helpers.root('tsconfig-aot.json') :
                            helpers.root('tsconfig.json')
                    }
                },
                'angular2-template-loader'
            ],
            exclude: [/node_modules/]
        },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file-loader?name=assets/[name].[ext]'
            },
            {   test: /\.css$/,
                use:  extractCSS.extract([ 'css-loader?url=false&minimize=true&autoprefixer=true', "postcss-loader"])
            },
            {
                test: /\.scss$/,
                use: extractSass.extract({
                    use: [
                        'css-loader?url=false&minimize=true',
                        'sass-loader?url=false'
                    ]
                })
            }
        ]
    },

    plugins: [


        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            jquery: 'jquery',
            "window.jQuery": "jquery"
        }),
        // Workaround for angular/angular#11580
        new webpack.ContextReplacementPlugin(/\@angular(\\|\/)core(\\|\/)esm5/,
            helpers.root('./'), // location of your src
            {} // a map of your routes
        ),

        new CopyWebpackPlugin([
            { from: './app/src/assets', to: 'assets', ignore: [ '**/*.css' ] }
        ]),

        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor', 'polyfills']
        }),

        new HtmlWebpackPlugin(),
        extractCSS,
        extractSass
    ]
};