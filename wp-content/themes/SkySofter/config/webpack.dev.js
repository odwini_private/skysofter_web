var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');
var webpack = require('webpack');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const ENV = (process.env.NODE_ENV = process.env.ENV = 'dev');

module.exports = webpackMerge(commonConfig, {
    devtool: 'cheap-module-eval-source-map',

    output: {
        path: helpers.root('app/dist'),
        publicPath: 'app/dist',
        filename: '[name].bundle.js',
        chunkFilename: '[id].chunk.js'
    },

    devServer: {
        historyApiFallback: true,
        stats: 'minimal'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                ENV: JSON.stringify(ENV)
            }
        }),

        new BrowserSyncPlugin({
                proxy: "localhost:8080",
                host: 'localhost',
                port: '3000',
                open: false,
                watchOptions: {
                    ignoreInitial: true,
                    ignored: ['**/*.scss', '**/*.css', '**/*.ts', '**/*.js']
                },
                files: [{
                    match: ['**/*.php'],
                    fn: function (event, file) {
                        if (event === "change") {
                            const bs = require('browser-sync').get('bs-webpack-plugin');
                            bs.reload();
                        }
                    }
                },
                    {
                    match: ['app/dist/**/*.css', 'app/dist/**/*.js'],
                    fn: function (event, file) {
                        if (event === "change") {
                             const bs = require('browser-sync').get('bs-webpack-plugin');
                             bs.stream();
                        }
                    }
                 }]
            },
            {
                injectChanges: true,
                reload: false,
            }),

        new webpack.NamedModulesPlugin()
    ]
});