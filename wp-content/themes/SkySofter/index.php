<?php
/**
 * The main template file
 *
 * This is the most generic template file in a SkySofter theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.SkySofter.org/Template_Hierarchy
 *
 * @package SkySofter
 * @subpackage Sky_Softer
 * @since Sky Softer 1.0
 */

get_header();


$news_options = array(
    'post_type'   => "post",
    'orderby'     => 'date',
    'order'       => 'DESC',
    'numberposts' => -1
);
$posts = get_posts($options);
if(isset($request['fields'])){
    foreach($posts as $post){
        $post = (array) $post;
        foreach ($request['fields'] as $field){
            $post[$field] = get_post_meta($post['ID'], $field, TRUE);
        }
        $post['thumbnail_url'] = get_the_post_thumbnail_url($post['ID']);
        if(isset($getCostumeFields) && !empty($getCostumeFields) && isset($request['custome_fields'])){
            $post['custome_fields'] = $this->getCostumeFields($post['ID'], $request['custome_fields']);
        }
        $news_response[] = $post;
    }
}else{
    foreach($posts as $post){
        $post = (array) $post;
        if(isset($getCostumeFields) && !empty($getCostumeFields) && isset($request['custome_fields'])){
            $post['custome_fields'] = $this->getCostumeFields($post['ID'],$request['custome_fields']);
        }
        $post['thumbnail_url'] = get_the_post_thumbnail_url($post['ID']);
        $news_response[] = $post;
    }
}

?>
<script>
    window.news = <?php new WP_REST_Response($news_response); ?>;
</script>
    <my-app></my-app>

<?php get_footer(); ?>
