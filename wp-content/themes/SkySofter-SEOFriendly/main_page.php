<?php
/**
 * Template Name: Main page
 *
 * @package SkySofter
 * @subpackage SkySofter
 * @since SkySofter 1.0
 */
get_header();


?>

    <?php
        get_template_part('template-parts/main-nav');
        get_template_part('template-parts/main/header');
        get_template_part('template-parts/main/services');
        get_template_part('template-parts/main/team');
        get_template_part('template-parts/main/case-study');
        get_template_part('template-parts/main/news');
        get_template_part('template-parts/main/clients-partners');
        get_template_part('template-parts/main/contact');
        get_template_part('template-parts/main/get-en-estimate');
    ?>



<?php get_footer(); ?>