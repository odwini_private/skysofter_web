<?php
/**
 * ScySofter functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in SkySofter to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.SkySofter.org/Theme_Development
 * @link https://codex.SkySofter.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.SkySofter.org/Plugin_API}
 *
 * @package SkySofter
 * @subpackage Sky_Softer
 * @since Sky Softer 1.0
 */

/**
 * ScySofter only works in SkySofter 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'skysofter_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various SkySofter features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own skysofter_setup() function to override in a child theme.
 *
 * @since Sky Softer 1.0
 */
function skysofter_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at SkySofter.org. See: https://translate.SkySofter.org/projects/wp-themes/skysofter
	 * If you're building a theme based on ScySofter, use a find and replace
	 * to change 'skysofter' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'skysofter' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let SkySofter manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect SkySofter to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since ScySofter 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://codex.SkySofter.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'skysofter' ),
		'social'  => __( 'Social Links Menu', 'skysofter' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.SkySofter.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', skysofter_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // skysofter_setup
add_action( 'after_setup_theme', 'skysofter_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Sky Softer 1.0
 */
function skysofter_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'skysofter_content_width', 840 );
}
add_action( 'after_setup_theme', 'skysofter_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.SkySofter.org/reference/functions/register_sidebar/
 *
 * @since Sky Softer 1.0
 */
function skysofter_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'skysofter' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'skysofter' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 1', 'skysofter' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'skysofter' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 2', 'skysofter' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'skysofter' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'skysofter_widgets_init' );

if ( ! function_exists( 'skysofter_fonts_url' ) ) :
/**
 * Register Google fonts for ScySofter.
 *
 * Create your own skysofter_fonts_url() function to override in a child theme.
 *
 * @since Sky Softer 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function skysofter_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'skysofter' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'skysofter' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'skysofter' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Sky Softer 1.0
 */
function skysofter_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'skysofter_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Sky Softer 1.0
 */
//function header_scripts() {
//    $themePath = get_template_directory_uri();
//    // app styles
//
//}
//add_action( 'wp_enqueue_scripts', 'header_scripts' );

/**
 *
 *SkySofter 1.0.0
 *Add scripts into the bottom of document
 *
*/
function footer_scripts() {

    $themePath = get_template_directory_uri();

    wp_enqueue_style( 'skysofter-vendor', $themePath . '/assets/dist/css/vendor.min.css', array(), '' );
    wp_enqueue_style( 'skysofter-common', $themePath . '/assets/dist/css/common.min.css', array(), '' );
    if(is_page_template("main_page.php")){
        wp_enqueue_style( 'skysofter-main', $themePath . '/assets/dist/css/main.min.css', array(), '' );
    }

    wp_enqueue_script( 'skysofter-vendor', $themePath . '/assets/dist/js/vendor.bundle.min.js', array(), '', true );
    wp_enqueue_script( 'skysofter-common', $themePath . '/assets/dist/js/common.bundle.min.js', array(), '', true );
    if(is_page_template("main_page.php")){
        wp_enqueue_script( 'skysofter-main', $themePath . '/assets/dist/js/main.bundle.min.js', array(), '', true );
    }
    wp_localize_script( 'skysofter-script', 'screenReaderText', array(
        'expand'   => __( 'expand child menu', 'skysofter' ),
        'collapse' => __( 'collapse child menu', 'skysofter' ),
    ) );
    wp_enqueue_style( 'skysofter-fontawesome', get_template_directory_uri() . '/assets/dist/css/font-awesome.min.css', array(), '' );
}

add_action('wp_footer', 'footer_scripts');



/**
 * Adds custom classes to the array of body classes.
 *
 * @since Sky Softer 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function skysofter_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'skysofter_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Sky Softer 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function skysofter_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Sky Softer 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function skysofter_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 840 <= $width ) {
		$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';
	}

	if ( 'page' === get_post_type() ) {
		if ( 840 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	} else {
		if ( 840 > $width && 600 <= $width ) {
			$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		} elseif ( 600 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'skysofter_content_image_sizes_attr', 10 , 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Sky Softer 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function skysofter_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		if ( is_active_sidebar( 'sidebar-1' ) ) {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		} else {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
		}
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'skysofter_post_thumbnail_sizes_attr', 10 , 3 );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since ScySofter 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function skysofter_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list'; 

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'skysofter_widget_tag_cloud_args' );


function getFields($request){
    if(isset($request['pageId'])){
        return requestResource($request);
    }else if(isset($request['postType'])){
        $getCostumeFields = $request['includeCostumeFields'];
        $response = array();
        $limit = -1;
        $order = 'DESC';
        if(isset($request['limit']) && !empty($request['limit'])){
            $limit = intval($request['limit']);
        }
        if(isset($request['order']) && !empty($request['order'])){
            $order = $request['order'];
        }
        $options = array(
            'post_type'   => $request['postType'],
            'orderby'     => 'date',
            'order'       => 'DESC',
            'numberposts' => $limit
        );
        $posts = get_posts($options);
        if(isset($request['fields'])){
            foreach($posts as $post){
                $post = (array) $post;
                foreach ($request['fields'] as $field){
                    $post[$field] = get_post_meta($post['ID'], $field, TRUE);
                }
                $post['thumbnail_url'] = get_the_post_thumbnail_url($post['ID']);
                if(isset($getCostumeFields) && !empty($getCostumeFields) && isset($request['custome_fields'])){
                    $post['custome_fields'] = getCostumeFields($post['ID'], $request['custome_fields']);
                }
                $response[] = $post;
            }
        }else{
            foreach($posts as $post){
                $post = (array) $post;
                if(isset($getCostumeFields) && !empty($getCostumeFields) && isset($request['custome_fields'])){
                    $post['custome_fields'] = getCostumeFields($post['ID'],$request['custome_fields']);
                }
                $post['thumbnail_url'] = get_the_post_thumbnail_url($post['ID']);
                $response[] = $post;
            }
        }
        return $response;
    }else{
        throw new Exception('Bad request');
    }
}



function requestResource($request){
    $pageId = $request['pageId'];
    $includebaseInfo = $request['includeBaseInfo'];
    $getCostumeFields = $request['includeCostumeFields'];

    if(isset($request['fields'])){
        $response = array();
        $fields = $request['fields'];
        foreach ($fields as $field){
            $response[] = get_post_meta($pageId, $field, TRUE);
        }
        if(isset($includebaseInfo) && !empty($includebaseInfo)){
            $response[] = get_post($pageId, '', false);
        }
        if(isset($getCostumeFields) && !empty($getCostumeFields)){
            $response["custome_fields"] = getCostumeFields($pageId, $request["custome_fields"]);
        }
        return $response;
    }else{
        $response = (array) get_post($pageId, '', false);
        if(isset($getCostumeFields) && !empty($getCostumeFields)){
            $response["custome_fields"] = getCostumeFields($pageId, $request["custome_fields"]);
        }
        return $response;
    }
}

 function getCostumeFields($postId, $fields){
    $response = array();
    if($fields){
        foreach ($fields as $field){
            $response[$field] = CFS()->get($field,$postId);
        }
    }
    return $response;
}