<?php
$template_url = get_template_directory_uri();

?>

<style>
    #page-loader{
        width: 100vw;
        height:100vh;
        position: fixed;
        top:0;
        left:0;
        z-index: 9999;
        display: block;
    }

</style>
<div id="page-loader">
    <img src='<?php echo $template_url;?>/assets/images/news_circle.png' alt="circles" class="rotated_circles rotate">
</div>
