<?php
$nav_items = wp_get_nav_menu_items("Top nav");
$template_url = get_template_directory_uri();
?>
<nav class="nav" >
    <div class="nav-container">
        <a class="logo" href="#">
            <img class="white_logo" src="<?php echo $template_url?>/assets/images/logo.png" >
            <img class="color_logo" src="<?php echo $template_url?>/assets/images/logo_big-color.png" >
        </a>
        <div class="nav-items">
            <?php foreach ($nav_items as $nav_item){ ?>
                <a class="nav-item"
                   href="<?php echo $nav_item->url;?>"
                   data-scrollTo="<?php echo $nav_item->attr_title;?>"
                ><?php echo $nav_item->title;?></a>
                <?php
            }?>
            <a class="get_an_estimate-button nav-button" href="#">Get an estimate</a>
            <!--<li class="nav-item"><a href="#">Get an estimate</a></li>-->
        </div>
        <div class="nav-burger">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</nav>

<div class="mobile_nav">
    <div class="mobile_menu-bg"></div>
    <div class="top-row">
        <a class="logo" href="#">
            <img src="<?php echo $template_url?>/assets/images/logo.png" >
        </a>
        <div class="close_menu"></div>
    </div>
    <div class="nav-items">
        <?php foreach ($nav_items as $nav_item){ ?>
            <a class="nav-item"
               href="<?php echo $nav_item->url;?>"
               data-scrollTo="<?php echo $nav_item->attr_title;?>"
            ><?php echo $nav_item->title;?></a>
            <?php
        }?>

        <a class="get_an_estimate-button nav-button" href="#">Get an estimate</a>
        <!--<li class="nav-item"><a href="#">Get an estimate</a></li>-->
    </div>
</div>