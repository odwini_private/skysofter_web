<?php
$projects = getFields(array(
    "postType" => "projects",
    "includeCostumeFields" => "true",
    "order" => "DESC",
    "custome_fields" => array(
        0 => 'project_short_description',
        1 => 'projects_category',
        2 => 'technologies'
    )
));
$template_url = get_template_directory_uri();
?>
<section class="case-study" id="case-study">
    <h1 class="case-study_heading container container-medium">Case Study</h1>
    <div class="case-study_content">
        <div class="case-study_content_bg"
             style="background-image: url(<?php echo $template_url; ?>/assets/images/projects-bg.jpg);"></div>
        <div class="case-study_content_filter">
            <div class="case-study_content_filter_item active" active-category="all">
                All
            </div>
<!--            <div class="case-study_content_filter_item">-->
<!---->
<!--            </div>-->
        </div>

        <div class="container container-medium content">
            <div class="circles">
                <img src="<?php echo $template_url; ?>/assets/images/elipses_case_study.png" alt="circles"
                     class="rotated_circles">
            </div>
            <div class="case-study_content_items">
                <?php foreach ($projects as $project) { ?>
                    <div project-category="<?php echo $project['custome_fields']["projects_category"] ?>" class="case-study_item">
                        <div class="case-study_item_bg"
                             style="background-image: url('<?php echo $project["thumbnail_url"] ?>');">
                        </div>
                        <div class="case-study_item_content">
                            <h3 class="case-study_item_heading">
                                <?php echo $project["post_title"]; ?>
                            </h3>
                            <div class="case-study_item_technologies">
                                <?php
                                $technologies = $project["custome_fields"]["technologies"];
                                foreach ($technologies as $kay => $technology) { ?>
                                    <div class="technologie">
                                        <?php if ($kay > 0) { ?><span>&nbsp;&nbsp;</span><?php }
                                        echo $technology['technology_name'] ?>
                                        <?php if ($kay + 1 < count($technologies)) { ?><span>,</span><?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
    <!--<div class="go-toButton">-->
    <!--<a class="button common">-->
    <!--<span>-->
    <!--Go to all projects-->
    <!--</span>-->
    <!--</a>-->
    <!--</div>-->
</section>

