<?php
$cliensPartners = getFields(array(
        "postType"=>"cliets_and_partners",
        "fields"=> array(
            0 => 'url'
        )
    )
);

?>

<div class="clientspartners" >
    <div class="container container-medium">
        <h2 class="heading">Clients & Partners</h2>
        <div class="clientspartners_items">
                <div class="owl-carousel">
                    <?php foreach ($cliensPartners as $cliensPartner){?>
                    <div  class="item_pairs_wrapp">
                        <a href="<?php echo $cliensPartner["url"]?>" target="_blank" rel="nofollow" class="clientspartners_item">
                            <img src="<?php echo $cliensPartner["thumbnail_url"];?>">
                        </a>
                    </div>
                    <?php } ?>
                </div>
        </div>
    </div>
    <div class="join-them get_an_estimate-button">
        <div class="container container-medium">
            <p>
                Do not hesitate, <a href="#">join them!</a>
            </p>
        </div>
    </div>
</div>
</div>