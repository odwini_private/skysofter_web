<?php

$newsHeading = CFS()->get("news_heading", 2);
$newsDescription = CFS()->get("news_description", 2);
$template_url = get_template_directory_uri();
$news_options = array(
    'post_type' => "post",
    'orderby' => 'date',
    'order' => 'DESC',
    'numberposts' => -1
);
$posts = get_posts($options);
?>
<div class="common_news-bg">
<div class="news " id="news">
    <div class="container container-medium ">
        <div class="circles circles_mobile">
            <img src="<?php echo $template_url; ?>/assets/images/circles.png" alt="circles" class="rotated_circles">
        </div>
        <div class="left-side">
            <div class="circles">
                <img src="<?php echo $template_url; ?>/assets/images/news_circle.png" alt="circles"
                     class="rotated_circles">
                <!--<circles [options]="{circles: 5, max_size: 45}">-->

                <!--</circles>-->
            </div>
            <h1><?php echo $newsHeading; ?></h1>
            <p>
                <?php echo $newsDescription; ?>
            </p>
            <!--<div class="go-toButton">-->
            <!--<a class="button common purple">-->
            <!--<span>-->
            <!--More news-->
            <!--</span>-->
            <!--</a>-->
            <!--</div>-->
        </div>
        <div class="right-side">
            <div class="news-slides">
                <div class="owl-carousel">
                    <?php foreach ($posts as $post) { ?>
                        <div class="owl-item"
                             style="background-image: url('<?php echo get_the_post_thumbnail_url($post); ?>');">
                            <div class="item-gradient">
                                <div class="item-gradient-hover"></div>
                            </div>
                            <div class="content">
                                <h4><?php echo $post->post_title; ?></h4>
                                <p class="date"><?php echo get_the_date("d M, Y", $post); ?></p>
                                <a class="read-more" href="#">
                                    Read more <span></span>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
                <div class="carousel_navigation">
                    <div class="left_arrow">
                        <svg xmlns="http://www.w3.org/2000/svg" width="73" height="45" viewBox="0 0 73 45">
                            <path d="M1.514,24.294L22.151,42.816a4.569,4.569,0,1,0,6.1-6.8L17.018,25.937H68.435a4.567,4.567,0,0,0,0-9.135h-51.9L28.14,7.045A4.567,4.567,0,0,0,22.265.051L1.627,17.395A4.571,4.571,0,0,0,0,20.822,4.8,4.8,0,0,0,1.514,24.294Z"/>
                        </svg>
                    </div>
                    <div class="hand-of-god">
                        <svg xmlns="http://www.w3.org/2000/svg" width="427" height="512" viewBox="0 0 427 512">
                            <path d="M384.3,192a42.477,42.477,0,0,0-21.621,5.885A42.643,42.643,0,0,0,292.8,170.052a42.675,42.675,0,0,0-57.951-14.99V42.667a42.7,42.7,0,0,0-85.4,0V328.146A10.68,10.68,0,0,1,134,337.688l-68.554-34.25A45.228,45.228,0,0,0,0,343.855V352a10.693,10.693,0,0,0,3.388,7.8L126.6,474.708A138.434,138.434,0,0,0,221.287,512h66.937C364.742,512,427,449.792,427,373.333V234.667A42.725,42.725,0,0,0,384.3,192Z"/>
                        </svg>
                    </div>
                    <div class="right_arrow">
                        <svg xmlns="http://www.w3.org/2000/svg" width="73" height="45" viewBox="0 0 73 45">
                            <path d="M71.486,19.179L50.849,0.682a4.565,4.565,0,1,0-6.1,6.794L55.982,17.538H4.565a4.561,4.561,0,1,0,0,9.122h51.9L44.86,36.4a4.564,4.564,0,1,0,5.876,6.985l20.637-17.32A4.562,4.562,0,0,0,73,22.646,4.791,4.791,0,0,0,71.486,19.179Z"/>
                        </svg>
                    </div>
                </div>
        </div>
    </div>
</div>
