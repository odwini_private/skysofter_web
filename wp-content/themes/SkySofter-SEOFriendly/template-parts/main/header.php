<?php
$postInfo = get_post('2', '', false);
$template_url = get_template_directory_uri();
?>

<section class="header" id="header">
    <div class="header-bg_floating-particles" style="background-image: url('<?php echo $template_url;?>/assets/images/stars.png');" ></div>
    <div class="header-bg">
        <div class="header-bg_image" style="background-image: url('<?php echo $template_url;?>/assets/images/header-bg.png');" ></div>
    </div>
    <div class="container container-medium">
        <div class="left-content">
            <div><?php echo $postInfo->post_content;?></div>
            <button class="get_an_estimate-button">
                <span>Hire us</span>
            </button>
        </div>
        <div class="right-content">
            <div class="sample">
                <!--<img src="{{themeHref}}/assets/images/Sales.png">-->
                <img class="bottom_pan" src="<?php echo $template_url;?>/assets/images/pans/sales_bottom-pan.png">
                <img class="middle_pan" src="<?php echo $template_url;?>/assets/images/pans/sales_middle-pan.png">
                <div class="top_pan">
                    <img class="top_pan-bg" src="<?php echo $template_url;?>/assets/images/pans/blank-top-pan.png">
                    <img class="top_pan-menu inner-pan" src="<?php echo $template_url;?>/assets/images/pans/side_menu-pan.png">
                    <img class="top_pan-top-menu inner-pan" src="<?php echo $template_url;?>/assets/images/pans/top_menu-pan.png">
                    <div class="frames-wrapp">
                        <img class="top_pan-main-frame inner-pan" src="<?php echo $template_url;?>/assets/images/pans/main-frame-pan.png">
                        <div class="frames-wrapp_right-pans">
                            <img class="top_pan-sales inner-pan" src="<?php echo $template_url;?>/assets/images/pans/sales-pan.png">
                            <img class="top_pan-statistics inner-pan" src="<?php echo $template_url;?>/assets/images/pans/statistics_pan.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
