<?php
$contact_informations = getFields(array(
        "pageId"=>"2",
        "includeCostumeFields"=>"true",
        "custome_fields"=> array(
            0 => 'main_social_links',
            1 => 'company_info'
        )
    )
);
$template_url = get_template_directory_uri();
?>

<div class="contact" id="contact">
    <div class="close_map">
        <span></span>
        <span></span>
    </div>
    <div class="google_map" id="google_map"></div>
    <div class="left-side">
        <div class="side-bg"></div>
        <div class="container-half container-half-medium">
            <div class="logo">
                <img  src="<?php echo $template_url;?>/assets/images/logo_big-gray.png">
            </div>
            <h3 class="contact_heading">Get in touch</h3>
            <p class="how-cen-we">How can we <span class="help-you">help you?</span></p>
            <form novalidate id="contactForm">
                <div class="input_group">
                    <label for="contactName">what is your name?</label>
                    <input  minlength="2" id="contactName" type="text" name="name" required>
<!--                    <p class="error" >*Name is required</p>-->
<!--                    <p class="error" >*Name must contain at least 2 chars</p>-->
                </div>
                <div class="input_group">
                    <label for="contactEmail">your email</label>
                    <input   formControlName="email" id="contactEmail" type="email" name="email" required>
<!--                    <p class="error" >*Email is required</p>-->
<!--                    <p class="error" >*The email address must contain at least the @ character</p>-->
                </div>
                <div class="input_group">
                    <label for="contactMessage">your message</label>
                    <textarea  minlength="8"  id="contactMessage" type="text" name="content" required></textarea>
<!--                    <p class="error" >*Description is required</p>-->
<!--                    <p class="error" >*Description must contain at least 8 chars</p>-->
                </div>
                <div class="input_group">
                    <button class="button" type="submit">Send</button>
                </div>
            </form>
        </div>
    </div>
    <!--<div class="contact_pin">-->
    <!--<img [src]="appThemeHref + '/assets/images/contact_pin.png'" alt="">-->
    <!--</div>-->
    <div class="middle_circles">
        <img class="nototate" src="<?php echo $template_url;?>/assets/images/contact_elipses.png">
        <img class="rotate" src="<?php echo $template_url;?>/assets/images/contact_stars.png">

    </div>
    <div class="right-side">
        <div class="side-bg"></div>
        <div class="container-half container-half-medium">
            <div class="social_media"  >
                <?php
                $socialIcons = $contact_informations['custome_fields']["main_social_links"];
                foreach ($socialIcons as $socialIcon){
                    ?>
                <a href="<?php echo $socialIcon["social_link_url"];?>" target="_blank" rel="nofollow"
                   class="fa <?php echo $socialIcon["social_link_icon_class"];?>">
                </a>
                <?php }?>
            </div>
            <div class="contact_info">
                <h3>Company info</h3>
                <div class="info">
                    <?php
                    $companyInformations = $contact_informations['custome_fields']["company_info"];
                    foreach ($companyInformations as $companyInformation){
                    ?>
                    <p>
                        <span><?php echo $companyInformation["info_title"];?></span>
                        <?php echo $companyInformation["info_content"];?>
                    </p>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
    <div class="show_map-targe-box"></div>
</div>


