<?php
$template_url = get_template_directory_uri();
?>
<div class="get-an-estimate" id="get-an-estimate" style="display: none;">
    <div class="close_estimate">

    </div>
    <div class="container container-medium">
        <div class="middle_circles">
            <img src='<?php echo $template_url;?>/assets/images/news_circle.png' alt="circles" class="rotated_circles rotate">
            <!--<circles [options]="{circles: 5, max_size: 45, min_size: 3.5, circlesColor: '#f2f2ff'}"></circles>-->
        </div>
        <form novalidate id="getAnEstimate">
            <h3 class="form-heading">Get an estimate in <span>3</span> days</h3>
            <p class="form-description">
                How can we <span>help you?</span>
            </p>
            <div class="input_group">
                <label for="contactName">what is your name?</label>
                <input minlength="2" id="contactName" type="text" name="name" required>
            </div>
            <div class="input_group">
                <label for="contactEmail">your email</label>
                <input id="contactEmail" type="email" name="email" required>
            </div>
            <div class="input_group">
                <label for="contactMessage">your message</label>
                <textarea minlength="8" id="contactMessage" type="text" name="content" required></textarea>
            </div>
            <div class="input_group">
                <button class="button" type="submit">Send</button>
            </div>
        </form>
    </div>
</div>

