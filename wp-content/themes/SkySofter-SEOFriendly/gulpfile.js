const gulp = require('gulp');
const browserify = require('browserify');
const babelify = require('babelify');
const babel = require('babel-register');
var source = require('vinyl-source-stream');
const transform = require('vinyl-transform');
const buffer = require('vinyl-buffer');
const gutil = require('gulp-util');
var glob = require('glob');
var runSequence = require('run-sequence');
const streamify = require('gulp-streamify')
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync');
const uglify = require('gulp-uglify');
const lodash = require('lodash');
const gulpCopy = require('gulp-copy');
const debug = require('gulp-debug');
const composer = require('gulp-uglify/composer');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const cache = require('gulp-cached');
const autoprefixer = require('gulp-autoprefixer');
const mocha = require('gulp-mocha');
const es = require('event-stream');
const watch = require('gulp-watch');

const sourceDir = 'assets/src/';
const distDir = './assets/dist/';

function buildJs(changedFiles) {
    let fileRelativeDir = changedFiles.path.substring((__dirname+"\\"+sourceDir + 'js\\').length);
    let firstCharIndex = (__dirname+"\\"+sourceDir + 'js\\').length;
    let lastCharIndex =  firstCharIndex + fileRelativeDir.indexOf('\\');

    if (fileRelativeDir.indexOf('\\') === -1) {
        lastCharIndex = changedFiles.path.indexOf('.js');
    }
    let bundleDir = changedFiles.path.substring(firstCharIndex, lastCharIndex);
    let sourceFile = './' + sourceDir + 'js/'+bundleDir+'.js';

    try {

        compileJsFile(sourceFile);
    }catch(e){
        // the file doesn't exists
        require('fs').accessSync(sourceFile);
        compileJsFile(changedFiles.path, "/"+bundleDir);
    }
};

function compileJsFile(sourceFile, dir = ""){
    glob(sourceFile ,  function (err, files) {
        if(typeof files[0] == undefined){
            return "already compiled";
        }
        if (err) files(err);
        return browserify({entries: [files[0]], debug: true})
            .on('error', function (err) {
                gutil.log(err.toString());
            })
            .transform(babelify, {presets: [
                ["env", {
                    "targets": {
                        // The % refers to the global coverage of users from browserslist
                        "browsers": [ ">0.80%"]
                    }
                }]
            ], compact: true, plugins: ['transform-es2015-classes']})
            .bundle()
            .on('error', gutil.log.bind(gutil, 'Browserify Error'))
            .pipe(source(files[0]))
            .pipe(streamify(uglify()))
            .pipe(rename({
                extname: '.bundle.min.js',
                dirname: 'js'+dir
            }))
            .pipe(gulp.dest(distDir))
            .pipe(browserSync.stream());
    })
}

gulp.task('js', function (done) {
    gutil.log('./'+sourceDir+'js/'+'*.js');
    gutil.log("____________________________");
    glob('./'+sourceDir+'js/'+'*.js', function (err, files) {
        if (err) done(err);
        var tasks = files.map(function (entry) {
            return browserify({entries: [entry], debug: true})
                .on('error', function (err) {
                    gutil.log(err.toString());
                })
                .transform(babelify, {presets: [
                    ["env", {
                        "targets": {
                            // The % refers to the global coverage of users from browserslist
                            "browsers": [ ">0.80%"]
                        }
                    }]
                ], compact: true, plugins: ['transform-es2015-classes']})
                .bundle()
                .on('error', gutil.log.bind(gutil, 'Browserify Error'))
                .pipe(source(entry))
                .pipe(streamify(uglify()))
                .pipe(rename({
                    extname: '.bundle.min.js',
                    dirname: 'js'
                }))
                .pipe(gulp.dest(distDir));
        });
        es.merge(tasks).on('end', done);
    })
});

gulp.task('css', function(){
    return gulp.src('./'+sourceDir+'css/**/*.css')
        .pipe(cache('cachedcss'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(distDir+'css'));
});

gulp.task('fonts', function(){
    return gulp.src('./'+sourceDir+'fonts/**/*')
        .pipe(gulp.dest(distDir+'fonts'));
});

gulp.task('sass', function () {
    return gulp.src('./'+sourceDir+'scss/**/*.scss')
        .pipe(sass({errLogToConsole: true}).on('error', sass.logError))
        .pipe(cache('cachedcss'))
        .pipe(autoprefixer())
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(distDir+'css'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function (file) {

    browserSync.init({
        proxy: "localhost/cervi_wordpress",
        port: 88
    });

    gulp.watch('./'+sourceDir+'js/**/*.js', function (files) {
        buildJs(files);
    });

    gulp.watch('./'+sourceDir+'fonts/**/*', ['fonts']);

    gulp.watch('./'+sourceDir+'css/**/*.css', ['css']);

    gulp.watch('./**/*.html', function (files) {
        browserSync.reload();
    });

    gulp.watch("./**/*.php").on('change', browserSync.reload);

    gulp.watch('./'+sourceDir+'scss/**/*.scss', ['sass']);

});

gulp.task("test", function(done){
    return gulp.src(['./assets/**/*.spec.js'], { read: false })
        .pipe(mocha({ reporter: 'list' }))
        .on('error', gutil.log);
});

gulp.task('tdd-single', function() {
    return gulp.watch('test/**/*.spec.js')
        .on('change', function(file) {
            gulp.src(file.path)
                .pipe(mocha({
                    compilers: babel
                }))
        });
});

gulp.task('build', function(done){
    runSequence(["sass", "js", "css", "fonts", "test"], done);
});

gulp.task('tdd', function() {
    return gulp.watch(['./**/*.spec.js'], ['tdd-single']);
});

gulp.task('default', ['watch']);
