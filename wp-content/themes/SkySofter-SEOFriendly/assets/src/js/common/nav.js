import {disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks} from 'body-scroll-lock';
import  * as ScrollTo from "./pagescroller";

class Nav {

    constructor() {
        this.navFixed = false;
        this.navScrolled = false;
        this.lastScrollPosition = window.pageYOffset;
        if (window.innerWidth <= 1250) {
            this.navFixed = false;
            $(".nav").removeClass("navFixed");
        } else {
            this.navFixed = false;
            $(".nav").removeClass("navFixed");
        }
        this.documentReady();
        this.pinEvents();
    }


    documentReady() {
        let that = this;
        $(document).ready(function () {
            $(".nav-burger").click(function () {
                $(".mobile_nav").fadeIn(300);
                disableBodyScroll($(".mobile_nav")[0]);
            });
            let menuScroller = new ScrollTo.ScrollTo({
                swingSpeedPerHeightUnit: 200,
                heightFix: 0
            });
            $(".nav-item, .nav-button, .close_menu").click(function () {
                $(".mobile_nav").fadeOut(300);
                enableBodyScroll($(".mobile_nav")[0]);
            });
            that.lastScrollPosition = window.pageYOffset;
            if (window.pageYOffset > 10) {
                that.navScrolled = true;
                $(".nav").addClass("navFixed");
            } else {
                that.navScrolled = false;
                that.navFixed = false;
                $(".nav").removeClass("navFixed");
            }
        });
    }

    pinEvents() {
        let that = this;
        $(window).resize(function () {
            that.onWindowResize();
        });
        $(document).scroll(function () {
            that.onWindowScroll();
        })
    }

    onWindowResize() {
        let that = this;
        if (window.innerWidth <= 1250) {
            that.navFixed = false;
            $(".nav").removeClass("navFixed");
        }
    }

    onWindowScroll() {
        let number = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        if (window.innerWidth < 1251) {
            this.mobileMenuScrollBack();
            if (number < 10) {
                $(".nav").removeClass("navFixed");
            } else {
                $(".nav").addClass("navFixed");
            }
        } else {
            let that = this;
            if (number > 100) {
                this.navFixed = true;
                $(".nav").addClass("navFixed");
            } else if (this.navFixed && number < 10) {
                this.navFixed = false;
                $(".nav").removeClass("navFixed");
            }
        }
    }

    mobileMenuScrollBack() {

        if (this.lastScrollPosition > (window.pageYOffset + 25) && this.navFixed === false) {
            this.lastScrollPosition = window.pageYOffset;
            this.navFixed = true;
            $(".nav").stop().fadeOut(function () {
                $(".nav").css("position", "fixed");
                $(".nav").fadeIn();
            });
        } else if (this.lastScrollPosition < (window.pageYOffset - 25) && this.navFixed === true) {
            this.navFixed = false;
            this.lastScrollPosition = window.pageYOffset;
            if ($(".nav").is(":visible")) {
                $(".nav").fadeIn(function () {
                    $(".nav").css("position", "absolute");
                    $(".nav").fadeOut();
                });
            }
        } else {
            this.lastScrollPosition = window.pageYOffset;
        }
        if (window.pageYOffset > 10) {
            this.navScrolled = true;
        } else {
            this.navScrolled = false;
            this.navFixed = false;
            $(".nav").css("position", "absolute");
            $(".nav").stop().fadeIn();
        }
    }

}

module.exports = Nav;