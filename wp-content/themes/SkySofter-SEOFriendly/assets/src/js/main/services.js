class Services{
    constructor(){
        this.initObject();
        this.initWindowSize = 1250;
        this.destroyWindowSize = 650;
    }

    initObject(){
        let that = this;
        $("document").ready(function(){
            if(window.innerWidth < that.destroyWindowSize ||  window.innerWidth > that.initWindowSize){
                that.carouselInited = false;
            }else if(window.innerWidth > that.destroyWindowSize && window.innerWidth < that.initWindowSize){
                that.initCarousel();
            }
            $(window).resize(function(){
                if(window.innerWidth < that.destroyWindowSize ||  window.innerWidth > that.initWindowSize){
                   if(that.carouselInited){
                       that.carousel.trigger('destroy.owl.carousel').removeClass('owl-loaded');
                       that.carousel.find('.owl-stage-outer').children().unwrap();
                       that.carouselInited = false;
                   }
                }
                if(window.innerWidth > that.destroyWindowSize && window.innerWidth < that.initWindowSize){
                    if(!that.carouselInited){
                        that.initCarousel();
                    }
                }
            });
        });

    }


    initCarousel(){
        this.carousel =  $(".services .owl-carousel").owlCarousel({
            navigation: false,
            pagination: false,
            stagePagging: 18,
            smartSpeed: 1200,
            fluidSpeed: 500,
            rewindNav: false,
            margin: 18,
            loop: true,
            items: 2.5,
            responsive: {
                750: {
                    items: 2.5
                },
                950: {
                    items: 3
                },
                1250: {
                    items: 4
                },
                1560: {
                    items: 4
                }
            }
        });
        this.carouselInited = true;
        this.pinEventsToCostumeArrows();
    }

    pinEventsToCostumeArrows() {
        let that = this;
        $(".services").find(".right_arrow").click(function () {
            that.carousel.trigger('next.owl.carousel');
        });
        $(".services").find(".left_arrow").click(function () {
            that.carousel.trigger('prev.owl.carousel');
        });

    }
}

module.exports = Services;