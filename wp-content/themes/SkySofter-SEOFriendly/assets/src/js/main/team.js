class Team{
    constructor(){
        this.initObject();
        this.initWindowSize = 1250;
        this.destroyWindowSize = 650;
    }

    initObject(){
        let that = this;
        $("document").ready(function(){
            if(window.innerWidth < that.destroyWindowSize ||  window.innerWidth > that.initWindowSize){
                that.carouselInited = false;
            }else if(window.innerWidth > that.destroyWindowSize && window.innerWidth < that.initWindowSize){
                that.initCarousel();
            }
            $(window).resize(function(){
                if(window.innerWidth < that.destroyWindowSize ||  window.innerWidth > that.initWindowSize){
                    if(that.carouselInited){
                        that.carousel.trigger('destroy.owl.carousel').removeClass('owl-loaded');
                        that.carousel.find('.owl-stage-outer').children().unwrap();
                        that.carouselInited = false;
                    }
                }
                if(window.innerWidth > that.destroyWindowSize && window.innerWidth < that.initWindowSize){
                    if(!that.carouselInited){
                        that.initCarousel();
                    }
                }
            });
        });
    }

    initCarousel(){
        this.carousel =  $(".team .owl-carousel").owlCarousel({
            navigation: false,
            pagination: false,
            smartSpeed: 1200,
            fluidSpeed: 500,
            rewindNav: false,
            margin: 20,
            stagePadding: 0,
            loop: true,
            items: 1.35,
            responsive: {
                650: {
                    items: 1.05
                },
                750:{
                    items: 1.15
                },
                850: {
                    items: 1.35
                },
                1000:{
                    items: 1.55
                },
                1100:{
                    items: 1.65
                },
                1250: {
                    items: 2.1
                },
                1560: {
                    items: 1.35
                }
            }
        });
        this.carouselInited = true;
        this.pinEventsToCostumeArrows();
    }
    pinEventsToCostumeArrows() {
        let that = this;
        $(".team").find(".right_arrow").click(function () {
            that.carousel.trigger('next.owl.carousel');
        });
        $(".team").find(".left_arrow").click(function () {
            that.carousel.trigger('prev.owl.carousel');
        });

    }
}

module.exports = Team;