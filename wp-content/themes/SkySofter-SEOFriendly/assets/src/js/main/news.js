class News{
    constructor(){
        this.initObject();
    }

    initObject(){
        let that = this;
        $("document").ready(function(){
             that.initCarousel();
        });
    }

    initCarousel(){
        this.carousel =  $(".news .owl-carousel").owlCarousel({
            navigation: true,
            pagination: false,
            smartSpeed: 1200,
            fluidSpeed: 500,
            rewindNav: false,
            margin: 25,
            loop: true,
            items: 2,
            responsive: {
                250: {
                    items: 1,
                    navigation: false,
                    pagination: true,
                    margin: 0
                },
                650:{
                    items: 1.3
                },
                750: {
                    items: 1.4
                },
                850:{
                    items: 1.6
                },
                1100: {
                    items: 1.8
                },
                1260: {
                    items: 2
                },
                1560: {
                    items: 2
                }
            }
        });
        this.carouselInited = true;
        this.pinEventsToCostumeArrows();
    }

    pinEventsToCostumeArrows() {
        let that = this;
        $(".news").find(".right_arrow").click(function () {
            that.carousel.trigger('next.owl.carousel');
        });
        $(".news").find(".left_arrow").click(function () {
            that.carousel.trigger('prev.owl.carousel');
        });

    }
}

module.exports = News;