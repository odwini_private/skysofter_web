class Contact {

    constructor() {
        let that = this;
        this.form = $("#contactForm").validate();
        $("#contactForm").submit(function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (that.form.valid()) {
                let value = $(this).serialize();
                $.post("mail.php", value)
                    .done(function (data) {
                        alert("Thank you for replay!");
                        $("#contactForm")[0].reset();
                    });
            }
        });
    }


}

module.exports = Contact;