import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';

class GetAnEstimate {

    constructor() {
        let that = this;
        this.form = $("#getAnEstimate").validate();
        $("#getAnEstimate").submit(function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (that.form.valid()) {
                let value = $(this).serialize();
                $.post("mail.php", value)
                    .done(function (data) {
                        alert("Thank you for replay!");
                        $(".get-an-estimate").fadeOut(300, function () {
                        });
                        enableBodyScroll($(".get-an-estimate")[0]);
                        $("body").removeClass("scroll-lock");
                        $("#getAnEstimate")[0].reset();
                    });
            }
        });

        $(".close_estimate").click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(".get-an-estimate").fadeOut(300, function () {
            });
            enableBodyScroll($(".get-an-estimate")[0]);
            $("body").removeClass("scroll-lock");

        });

        $(".get_an_estimate-button").click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(".get-an-estimate").fadeIn(300, function () {
                disableBodyScroll($(".get-an-estimate")[0]);
                $("body").toggleClass("scroll-lock");
            });
        });
    }


}

module.exports = GetAnEstimate;