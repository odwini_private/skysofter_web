class clentsAndPartners{
    constructor(){
        this.initObject();
    }

    initObject(){
        $(function(){
            let destroyWindowSize = 650;
            let clientsCarousel = $(".clientspartners .owl-carousel").owlCarousel({
                navigation: false,
                pagination: false,
                loop: false,
                smartSpeed: 1200,
                fluidSpeed: 500,
                rewindNav: false,
                margin: 25,
                items: 1,
                responsive: {
                    750: {
                        items: 2
                    },
                    950: {
                        items: 3
                    },
                    1250: {
                        items: 4
                    },
                    1560: {
                        items: 5
                    }
                }
            });

            $(window).resize(function(){
                if(window.innerWidth < destroyWindowSize){
                    clientsCarousel.trigger('destroy.owl.carousel').removeClass('owl-loaded');
                    clientsCarousel.find('.owl-stage-outer').children().unwrap();
                }
            });
        });
    }
}

module.exports = clentsAndPartners;