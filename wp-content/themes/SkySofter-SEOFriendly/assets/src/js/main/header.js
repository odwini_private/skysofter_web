class Header{
    constructor(){
        this.header = $(".header");
        this.pinEvents();
        this.documentLoads();
    }

    documentLoads(){
        let that = this;
        $(document).ready(function(){
            if(window.innerWidth > 1250){
                that.setSizeOfHeader();
            }else{
                that.removeSizeOfHeader();
            }
            that.header.find(".sample").addClass("make-magic");
        });
    }

    setSizeOfHeader(){
        let height =this.header.height();
        this.header.css("min-height", height+"px");
    }

    removeSizeOfHeader(){
        this.header.css("min-height", "");
    }

    pinEvents(){
        let that = this;
        $(window).resize(function(){
            that.onWindowResize();
        });
        $(document).scroll(function(){
            that.onWindowScroll();
        })
    }

    onWindowResize(){
        if(window.innerWidth > 1250){
            this.setSizeOfHeader();
        }else{
            this.removeSizeOfHeader();
        }
    }

    onWindowScroll() {
        let number = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        if (number > 30) {
            $(".header .right-content").addClass("animated");
        }
    }


}

module.exports = Header;