class CaseStudy {

    constructor() {
        this.projectsElements = $(".case-study_item");
        this.categories = this.getCategories();
        this.buildCategories();
        this.categoriesDOM = $(".case-study_content_filter").find(".case-study_content_filter_item");
        this.pinEventsToCategories();
    }

    pinEventsToCategories() {
        let that = this;
        this.categoriesDOM.click(function () {
            that.setActiveCategory(this);
        });
    }

    buildCategories() {
        for (let i = 0; i < Object.keys(this.categories).length; i++) {
            let objectKey = Object.keys(this.categories)[i];
            $(".case-study_content_filter").append(this.categories[objectKey].element);
        }
    }

    setActiveCategory(element) {
        this.categoriesDOM.removeClass("active");
        $(element).addClass('active');
        let activeCategory = $(element).attr("active-category");
        if (activeCategory !== 'all') {
            this.projectsElements.each(function () {
                if ($(this).attr('project-category') !== activeCategory) {
                    $(this).fadeOut(0);
                }else{
                    if(!$(this).is(":visible")){
                        $(this).fadeIn(0);
                    }
                }
            });
        } else {
            this.projectsElements.each(function () {
                if(!$(this).is(":visible")){
                    $(this).fadeIn(0);
                }
            });
        }
    }

    getCategories() {
        let categories = {};
        this.projectsElements.each(function () {
            let category = $(this).attr("project-category").toLowerCase();
            let element = document.createElement("div");
            element.classList.add('case-study_content_filter_item');
            element.setAttribute('active-category', category);
            element.textContent = category;
            if (categories[category] === undefined || categories[category] === null) {
                categories[category] = {
                    element: element,
                    name: category
                };
            }
        });
        return categories;
    }

}

module.exports = CaseStudy;