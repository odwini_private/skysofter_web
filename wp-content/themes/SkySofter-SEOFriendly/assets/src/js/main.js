import Header from "./main/header";
let header = new Header();

import Services from "./main/services";
let services = new Services();

import Team from "./main/team";
let team = new Team();

import CaseStudy from  "./main/case-study";
let caseStudy = new CaseStudy();

import News from  "./main/news";
let news = new News();

import clentsAndPartners from "./main/clentsAndPartners";
let clients = new clentsAndPartners();

import Contact from "./main/contact.js";
let contact = new Contact();

import GetAnEstimate from "./main/getAndEstimate.js";
let getAnEstimate = new GetAnEstimate();

